-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-02-2019 a las 21:52:27
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `radio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audio`
--

CREATE TABLE IF NOT EXISTS `audio` (
  `id` int(10) NOT NULL,
  `nombre_audio` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_audio` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_imagen` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_imagen` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `directorio` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `predeterminado` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `borrado` varchar(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `audio`
--

INSERT INTO `audio` (`id`, `nombre_audio`, `direccion_audio`, `nombre_imagen`, `direccion_imagen`, `directorio`, `predeterminado`, `borrado`) VALUES
(4, 'Audio 10', 'programas/Programa_1/350e1401   Boston   More Than A Feeling.mp3', 'Maria Julia0', 'programas/Programa_1/44cf7alogo american mathematical society.jpg', 'programas/Programa_1', '0', '0'),
(5, 'Audio 1', 'programas/Programa_2/732cebAmapola_mod_5_final.mp3', 'Maria Julia', 'programas/Programa_2/1541775479274383.jpg', 'programas/Programa_2', '1', '0'),
(6, 'Audio 1', 'programas/Programa_3/la ballena.mp3', 'Maria Julia', 'programas/Programa_3/1541775479274383.jpg', 'programas/Programa_3', '0', '0'),
(7, 'Audio 1', 'programas/e44fdbPrograma_4/7836e8la ballena.mp3', 'Maria Julia', 'programas/e44fdbPrograma_4/605535mapeo_por_cuerpos_todos.jpg', 'programas/e44fdbPrograma_4', '0', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correo`
--

CREATE TABLE IF NOT EXISTS `correo` (
  `id` int(10) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `borrado` varchar(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `correo`
--

INSERT INTO `correo` (`id`, `nombre`, `correo`, `borrado`) VALUES
(1, 'JesÃºs Ventura', 'jesusrvm@yahoo.com.ar', '1'),
(2, 'JesÃºs', 'correoasdf@gsad.comsdf', '1'),
(3, 'sfdgsdfg', '', '1'),
(4, 'Programa_10', 'correoasdf@gsad.comsdf', '1'),
(5, 'Programa_10', 'correoasdf@gsad.comsdf', '1'),
(6, 'Programa_10', 'correoasdf@gsad.comsdf', '1'),
(7, 'Programa_10', 'correoasdf@gsad.comsdf', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otros_datos`
--

CREATE TABLE IF NOT EXISTS `otros_datos` (
  `id` int(5) NOT NULL,
  `programas_radio` int(10) NOT NULL,
  `audios` int(10) NOT NULL,
  `videos` int(10) NOT NULL,
  `post` int(10) NOT NULL,
  `suscriptores` int(10) NOT NULL,
  `color_1` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `color_2` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_pagina` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `texto_1` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `texto_2` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `texto_3` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `texto_4` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `texto_5` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `texto_6` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `texto_7` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `texto_8` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `texto_9` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `texto_10` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `texto_11` varchar(500) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `otros_datos`
--

INSERT INTO `otros_datos` (`id`, `programas_radio`, `audios`, `videos`, `post`, `suscriptores`, `color_1`, `color_2`, `direccion_pagina`, `texto_1`, `texto_2`, `texto_3`, `texto_4`, `texto_5`, `texto_6`, `texto_7`, `texto_8`, `texto_9`, `texto_10`, `texto_11`) VALUES
(1, 15, 4, 2, 3, 1, '#1cb762', '#dc0ced', 'http://www.example.com/', 'Programas', 'Ver todo', 'Post', 'Ver todo', 'Blogs', 'Programas de radio', 'Estamos aquÃ­', 'EscrÃ­banos desde el siguiente formulario', 'Posts recientes', 'Top programas mÃ¡s escuchados', 'Otros programas de radio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(10) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `parrafo_1` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `parrafo_2` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `parrafo_3` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_imagen` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `codigo` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `numero` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `anio` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `mes` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `dia` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `directorio` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `borrado` varchar(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `post`
--

INSERT INTO `post` (`id`, `nombre`, `parrafo_1`, `parrafo_2`, `parrafo_3`, `imagen`, `direccion_imagen`, `codigo`, `numero`, `anio`, `mes`, `dia`, `directorio`, `borrado`) VALUES
(7, 'Programa_10', 'Hola', 'CÃ³mo', 'Va', 'Imagen 1', 'post/bf7fb5Programa_10/656422el cine y su musica videos   3.jpg', 'https://www.youtube.com/embed/dzyT3NmNNgA', '1', '2018', '12', '10', 'post/bf7fb5Programa_10', '1'),
(8, 'Programa 100', 'Hola', 'CÃ³mo', 'Va', 'Maria Julia0', 'post/e4327fPrograma_1/8cb91aFoto Circulacion con cuadro.jpg', 'https://www.youtube.com/embed/eM2IXmfFWEM', '1000', '2018', '12', '11', 'post/e4327fPrograma_1', '1'),
(9, 'Programa 10', 'Hola', 'CÃ³mo', 'Va', 'Imagen 1', 'post/bfc3a0Programa_10/0ee48b1546273_549191445178053_1839772593_n.jpg', 'https://www.facebook.com/media/set/?set=a.2200542639971576&type=1&l=1719626cee', '50', '2018', '12', '11', 'post/bfc3a0Programa_10', '1'),
(10, 'Programa 1', 'Hola', 'CÃ³mo', 'Va', 'Imagen 1', 'post/52c972Programa_1/f85264biblioteca antonio monteiro imagen para redes sociales.jpg', 'https://www.youtube.com/watch?v=7yTw4w_xCdU', '33', '2018', '12', '11', 'post/52c972Programa_1', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `radio`
--

CREATE TABLE IF NOT EXISTS `radio` (
  `id` int(10) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `numero` int(10) NOT NULL,
  `enlace` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_imagen` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_imagen` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `borrado` varchar(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `radio`
--

INSERT INTO `radio` (`id`, `nombre`, `numero`, `enlace`, `descripcion`, `fecha`, `nombre_imagen`, `direccion_imagen`, `borrado`) VALUES
(1, 'ElecciÃ³n de Ortiz', 98, 'https://ar.ivoox.com/es/98-eleccion-ortiz-25-10-18-audios-mp3_rf_30680430_1.html', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '[25/10/18]', 'Imagen 1a', 'programas_radio/a178b0Eleccion de Ortiz/90c927Mapa casas congreso3.jpg', '0'),
(2, 'Reforma en Cuba', 99, 'https://ar.ivoox.com/es/99-reforma-cuba-01-11-18-audios-mp3_rf_30680441_1.html', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '[01/11/18]', 'Imagen 1', 'programas_radio/a99e2fReforma en Cuba/58733aDia del Estudiante  1966.JPG', '0'),
(3, 'Aniversario de los 100 programas', 100, 'https://ar.ivoox.com/es/30680452', '', '[8/11/18] ', 'Imagen1', 'programas_radio/ba2600Aniversario de los 100 programas/9b3880A_ConMdeMemoria.jpg', '0'),
(4, 'Agnes Benedeck', 97, 'https://ar.ivoox.com/es/97-agnes-benedeck-11-10-18-audios-mp3_rf_30660935_1.html', '', '[11/10/18]', 'Imagen 1', 'programas_radio/bb1375Agnes Benedeck/3c37fcA_ConMdeMemoria.jpg', '0'),
(5, 'Reforma en Chile', 96, 'https://ar.ivoox.com/es/96-reforma-chile-04-10-18-audios-mp3_rf_30649883_1.html', '', '[04/10/18]', 'Imagen 1', 'programas_radio/0f2d3bReforma en Chile/c61df1A_ConMdeMemoria.jpg', '0'),
(6, 'Aniversario de Funciones Reales ', 95, 'https://ar.ivoox.com/es/95-aniversario-funciones-reales-27-09-18-audios-mp3_rf_30648221_1.html', '', '[27/09/18] ', 'Imagen 1', 'programas_radio/4104cfAniversario de Funciones Reales/cf388fA_ConMdeMemoria.jpg', '0'),
(7, 'Desaparecidos Bahienses', 94, 'https://ar.ivoox.com/es/94-desaparecidos-bahienses-20-09-18-audios-mp3_rf_30647430_1.html', '', '[20/09/18]', 'Imagen 1', 'programas_radio/fc78edDesaparecidos Bahienses/b06784A_ConMdeMemoria.jpg', '0'),
(8, 'La Noche de los LÃ¡pices', 93, 'https://ar.ivoox.com/es/93-la-noche-lapices-13-09-18-audios-mp3_rf_30616572_1.html', '', '[13/09/18]', 'Imagen 1', 'programas_radio/3e0d56La Noche de los Lapices/48cb16A_ConMdeMemoria.jpg', '0'),
(9, 'Con M de Memoria', 92, 'https://ar.ivoox.com/es/92-06-09-18-audios-mp3_rf_30615940_1.html', '', '[06/09/18] ', 'Imagen 1', 'programas_radio/cc56a3Con M de Memoria/93ba4cA_ConMdeMemoria.jpg', '0'),
(10, 'Josefina Mendoza', 91, 'https://ar.ivoox.com/es/91-josefina-mendoza-reformista-30-08-18-audios-mp3_rf_30611340_1.html', '', '[30/08/18]', 'Imagen 1', 'programas_radio/8506fdJosefina Mendoza   reformista /030989A_ConMdeMemoria.jpg', '0'),
(11, 'Mujeres de la reforma', 90, 'https://ar.ivoox.com/es/90-mujeres-reforma-23-08-18-audios-mp3_rf_30611293_1.html', '', '[23/08/18]', 'Imagen 1', 'programas_radio/dfdb9eMujeres de la reforma/d3da41A_ConMdeMemoria.jpg', '0'),
(12, 'Con M de Memoria', 89, 'https://ar.ivoox.com/es/89-16-08-18-audios-mp3_rf_30611075_1.html', '', '[16/08/18]', 'Imagen 1', 'programas_radio/7ab9e8Con M de Memoria/ea8eb5A_ConMdeMemoria.jpg', '0'),
(13, 'La expansiÃ³n nacional', 88, 'https://ar.ivoox.com/es/player_ej_30591424_2_1.html?data=lJWim5aYdpWhhpywj5qaaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncZmsj5C5w5DJvNHVz9jWh6iXaaOnz5Dbw8jNs8_VzZDRx9GPsdDqytLWx9PYs4zmxsvc1NLNt9XVjIqipJWRaZi3jqjc0JCxb8XZjLLSz9TWrcKhhpywj5k.&', '', '[09/08/18]', 'Imagen 1', 'programas_radio/49dadfLa expansion nacional del movimiento reformista/947177A_ConMdeMemoria.jpg', '0'),
(14, 'Edgardo Fernandez', 87, '<iframe width="100%" height="200" frameborder="0" allowfullscreen="" scrolling="no" src="https://ar.ivoox.com/es/player_ej_30591420_2_1.html?data=lJWim5aYdpGhhpywj5qZaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5yncZmrj5CyxszFtsXjjKvS1NPFssXZ25DA1sbHp9CfhpqvkpqJdqekmIqfqJacaZa4joqkpZKns8-frpDRx5Cxqc7j087Oj4qbh46o&"></iframe>', '', '[05/07/18]', 'Imagen 1', 'programas_radio/0c79a0Edgardo Fernandez Stacco/9d15bdA_ConMdeMemoria.jpg', '0'),
(15, 'IntervenciÃ³n de Yrigoyen', 86, '<iframe width="100%" height="200" frameborder="0" allowfullscreen="" scrolling="no" src="https://ar.ivoox.com/es/player_ej_30591418_2_1.html?data=lJWim5aYdZmhhpywj5qaaZS1kpaah5yncZOhhpywj5WRaZi3jpaah5yncZmqj5C20NnJttfZz8jWh6iXaaOnz5DRx5C9tsrb0N7S0JCJeaOmmYqfqJWaaZO6kp2Sl6mRaZi3jqjc0JCxb8XZjLLSz9TWrcKhhpywj5k.&"></iframe>', '', '[28/06/18]', 'Imagen 1', 'programas_radio/2724c5Intervencion de Yrigoyen/93dc88A_ConMdeMemoria.jpg', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `top_programas`
--

CREATE TABLE IF NOT EXISTS `top_programas` (
  `id` int(10) NOT NULL,
  `id_programa` int(10) NOT NULL,
  `nro_programa` int(10) NOT NULL,
  `contador` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `top_programas`
--

INSERT INTO `top_programas` (`id`, `id_programa`, `nro_programa`, `contador`) VALUES
(4, 15, 86, 14),
(5, 14, 87, 8),
(6, 13, 88, 4),
(7, 4, 97, 1),
(8, 10, 91, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(10) NOT NULL,
  `usuario` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `habilitado` varchar(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `contrasena`, `habilitado`) VALUES
(1, 'admin', 'admin', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `codigo` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `borrado` varchar(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `video`
--

INSERT INTO `video` (`id`, `nombre`, `codigo`, `tipo`, `borrado`) VALUES
(1, 'Lista 1', 'Ingrese el cÃ³digo...', '', '1'),
(2, 'aasasdf', 'asdfasdf', '', '1'),
(3, 'asdfasdf', 'https://www.youtube.com/embed/videoseries?list=PLWAfFL2C7zsbPoTKhSQjd36xmxgJR-tkF', '', '1'),
(4, 'Video 1', 'PLWAfFL2C7zsbPoTKhSQjd36xmxgJR-tkF', '2', '0'),
(5, 'Video 2', 'https://www.youtube.com/embed/eM2IXmfFWEM', '1', '0');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `audio`
--
ALTER TABLE `audio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `correo`
--
ALTER TABLE `correo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `otros_datos`
--
ALTER TABLE `otros_datos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `radio`
--
ALTER TABLE `radio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `top_programas`
--
ALTER TABLE `top_programas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `audio`
--
ALTER TABLE `audio`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `correo`
--
ALTER TABLE `correo`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `otros_datos`
--
ALTER TABLE `otros_datos`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `post`
--
ALTER TABLE `post`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `radio`
--
ALTER TABLE `radio`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `top_programas`
--
ALTER TABLE `top_programas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
      
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
        <title>Control de inventario</title>
		<!-- Estilos para la pantalla de logueo, registro, fondo y letras-->
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
        <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/animate-custom.css" />
		<link rel="shortcut icon" href="images/hd_klemmbrett.png" type="image/png" />
		<script src="js/custom.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>    
        <link rel="stylesheet" type="text/css" href="css/agregados.css" />
	</head>
    <body>
		<?php
        //phpinfo();
        //unlink('install.php');//Descomentar para borrar el archivo instalador en la versión para distribución
        require_once('connection.php');
        $sql=" select idioma from basedd where id=1";
        //require_once('res.php');
        $res = mysqli_query($dbh,$sql);
        $res2 = mysqli_fetch_array($res);
        $idioma=$res2['idioma'];
        if ($idioma=="esp"){
            require_once('esp.php');
        } elseif ($idioma=="eng") {
            require_once('eng.php');
        }
        ?>
		<br><br><br><br><br>
		<div class="container">
                      
            <section>				
                <div id="container_demo" >
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
						<!--Sección de logueo-->
                        <div id="login" class="animate form">
							<a name="ingresar">
                            <form  action="ingreso.php" autocomplete="on" method="POST"> 
                                <h1>Control de inventario</h1>
								<p> 
                                    <label for="usuario" class="uname" data-icon="u" ><?php echo $v22;?></label>
                                    <input id="usuario" name="usuario" required="required" type="text" placeholder="miusuario" autofocus/>
                                </p>
                                <p> 
                                    <label for="contraseña" class="youpasswd" data-icon="p"><?php echo $v23;?></label>
                                    <input id="contraseña" name="contraseña" required="required" type="password" placeholder="********" /> 
                                </p>
                                <p class="keeplogin"> 
									<input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" /> 
									<label for="loginkeeping"><?php echo $v24;?></label>
								</p>
                                <p class="login button"> 
                                    <input name="ingresar" type="submit" value="<?php echo $v25;?>" /> 
								</p>
                                
                            </form>
							<?php
							if (isset($_REQUEST['exito'])) {
								$exito = $_REQUEST['exito'];
							} else {
								$exito = "";
							}
							if ($exito=="4"){
								$mensaje=$v237;
								echo "<script>";
								echo "alert('$mensaje');";  
								echo "</script>";
							}
							?>
							</a>
                        </div>
                           
                        <!-- Sección de registro nuevo usuario-->
                        <a name="registro">
                        <div id="register" class="animate form">
                            <form action="registro.php" autocomplete="on" method="post"> 
                                <h1><?php echo $v232;?></h1> 
                                <p> 
                                    <label for="nombre" class="uname" data-icon="u"><?php echo $v36;?></label>
                                    <?php if (isset($_GET['nombre'])){?>
                                        <input id="nombre" name="nombre" required="required" type="text" value="<?php echo $_GET['nombre'];?>" />
                                    <?php } else {?>
                                        <input id="nombre" name="nombre" required="required" type="text" placeholder="minombre" /><?php
                                    }?>	
                                </p>
                                <p> 
                                    <label for="apellido" class="uname" data-icon="u"><?php echo $v235;?></label>
                                    <?php if (isset($_GET['apellido'])){?>
                                        <input id="apellido" name="apellido" required="required" type="text" value="<?php echo $_GET['apellido'];?>" />
                                    <?php } else { ?>
                                        <input id="apellido" name="apellido" required="required" type="text" placeholder="miapellido" /><?php
                                    }?>
                                </p>
                                <p> 
                                    <label for="usuario" class="uname" data-icon="u"><?php echo $v27;?></label>
                                    <!-- con isset($_GET['usuario']) se toma valor enviado desde registro para volver a mostrar los valores ingresados al registrarse en caso de error en el registro-->									
                                    <?php if (isset($_GET['usuario'])) {?>
                                        <input id="usuario" name="usuario" required="required" type="text" value="<?php echo $_GET['usuario'];?>"/>
                                    <?php } else { ?>
                                            <input id="usuario" name="usuario" required="required" type="text" placeholder="miusuario690" /><?php
                                        }?>
                                        
                                </p>
                                <p> 
                                    <label for="contraseña" class="youpasswd" data-icon="p"><?php echo $v236;?></label>
                                    <?php if (isset($_GET['contraseña'])){?>
                                        <input id="contraseña" name="contraseña" required="required" type="text" value="<?php echo $_GET['contraseña'];?>"/>
                                    <?php } else { ?>
                                        <input id="contraseña" name="contraseña" required="required" type="text" placeholder="ej. X8df!90EO"/><?php
                                    } ?>
                                </p>
                                <p class="signin button"> 
                                    <input name="submit" type="submit" value="<?php echo $v233;?>"/> 
                                </p>
                                <p class="change_link">  
                                    <a href="cerrar ventana.php" class="to_register"><?php echo $v234;?></a>
                                </p>
                            </form>
                        </div>
                        </a>
                    </div>
                </div>  
            </section>
        </div>
	</body>
</html>
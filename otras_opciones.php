<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Otras opciones | Con "M" de memoria</title>
<?php
require_once('meta.php');
require_once('connection.php');
?>
</head> 
   <!-- /w3layouts -->
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
        
    <section>
      <!-- left side start-->
		<?php
        require_once('menu.php');
        ?>
		<!-- left side end-->
		 <!-- /agileinfo -->
					<!-- app-->
			<?php
            require_once('app.php');
            ?>
			<!-- //app-->
			 <!-- /agile-its -->
		<!-- signup -->
			<?php
            require_once('registro.php');
            ?>
			<!-- //signup -->
	 <!-- /w3layouts-agile -->
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php
            require_once('cabecera.php');
            ?>
			<!--<pre><?php //echo "Nombre post: ".$nombre_post." - anio post: ".$anio_post." - mes post: ".$mes_post." id post: ".$id_post;?></pre>-->
					 <!-- /w3l-agile -->
				<!--notification menu end -->
				<!-- //header-ends -->
					<div id="page-wrapper">
                        <div class="inner-content">
                                                        
                            <div class="typography">
                                <div class="grid_3 grid_5">
                                    <div class="col-md-12">
                                        <div class="list-group list-group-alternate"> 
                                            <a href="otras_opciones.php?texto=1" class="list-group-item"><i class="ti ti-email"></i>Títulos de páginas</a>
                                            <a href="otras_opciones.php?color=1" class="list-group-item"><i class="ti ti-email"></i>Cambiar colores</a>
                                            <a href="otras_opciones.php?direccion=1" class="list-group-item"><i class="ti ti-email"></i>Cambiar dirección de página</a>
                                        </div>
                                   </div>
                                   <div class="clearfix"> </div>
                                </div>
                            </div>
                            <?php
                            if (isset($_REQUEST['direccion'])) {
                                $direccion = $_REQUEST['direccion'];
                            } else {
                                $direccion = "";
                            }
                            if ($direccion == "1"){
                                $sql="select direccion_pagina from otros_datos where id=1";
                                $res = mysql_query($sql);
                                $res2 = mysql_fetch_array($res);
                                ?>
                                <form action="funciones.php" method="post">
                                <div class="input-group">
								  <span class="input-group-addon" id="basic-addon1">Dirección raiz de la página web - Ejemplo: http://www.example.com/</span>
                                  <div class="clearfix"> </div>
								</div>
                                <div class="form-group">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <input type="text" name="direccion_pagina" class="form-control1" value="<?php echo $res2['direccion_pagina'];?>" autofocus>
                                      </div>
                                    <div class="clearfix"> </div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="hidden" name="otras_opciones" value="1"/>
                                            <input type="hidden" name="direccion" value="1"/>
                                            <input type="submit" value="enviar_direccion">
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                </div>
                                </form>
                                <?php
                            }    
                            if (isset($_REQUEST['color'])) {
                                $color = $_REQUEST['color'];
                            } else {
                                $color = "";
                            }
                            if ($color == "1"){
                                $sql="select color_1, color_2 from otros_datos where id=1";
                                $datos=mysql_query($sql,$dbh);
                                while ($row=mysql_fetch_array($datos)) {
                                    $color_1 = $row['color_1'];
                                    $color_2 = $row['color_2'];
                                }
                                mysql_free_result($datos);
                                ?>
                                <form action="funciones.php" method="post">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4 grid_box1">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1">Selecciona tu color favorito:</span>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <!--<input type="color" name="favcolor" value="#ff0000">-->
                                                <input type="color" name="favcolor_1" value="<?php echo $color_1;?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4 grid_box1">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1">Selecciona tu color favorito:</span>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <!--<input type="color" name="favcolor" value="#ff0000">-->
                                                <input type="color" name="favcolor_2" value="<?php echo $color_2;?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="hidden" name="otras_opciones" value="1"/>
                                            <input type="hidden" name="color" value="1"/>
                                            <input type="submit" value="enviar_color">
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                </div>
                                </form>
                            <?php    
                            }
                            if (isset($_REQUEST['texto'])) {
                                $texto = $_REQUEST['texto'];
                            } else {
                                $texto = "";
                            }
                            $texto_1 = "";
                            $texto_2 = "";
                            $texto_3 = "";
                            $texto_4 = "";
                            $texto_5 = "";
                            $texto_6 = "";
                            $texto_7 = "";
                            $texto_8 = "";
                            $texto_9 = "";
                            $texto_10 = "";
                            $texto_11 = "";
                            if ($texto == "1"){
                                $sql="select texto_1, texto_2, texto_3, texto_4, texto_5, texto_6, texto_7, texto_8, texto_9, texto_10, texto_11 from otros_datos where id=1";
                                $datos=mysql_query($sql,$dbh);
                                while ($row=mysql_fetch_array($datos)) {
                                    $texto_1 = $row['texto_1'];
                                    $texto_2 = $row['texto_2'];
                                    $texto_3 = $row['texto_3'];
                                    $texto_4 = $row['texto_4'];
                                    $texto_5 = $row['texto_5'];
                                    $texto_6 = $row['texto_6'];
                                    $texto_7 = $row['texto_7'];
                                    $texto_8 = $row['texto_8'];
                                    $texto_9 = $row['texto_9'];
                                    $texto_10 = $row['texto_10'];
                                    $texto_11 = $row['texto_11'];
                                }
                                mysql_free_result($datos);
                                ?>
                                <div class="typography">
                                    <form action="funciones.php" method="post">  
                                   <div class="grid_3 grid_4">
                                     
                                     <div class="bs-example">
                                         <table class="table">
                                          <tbody>
                                            <tr>
                                              <td><h1 id="h1.-bootstrap-heading">Texto de página de inicio<a class="anchorjs-link" href="#h1.-bootstrap-heading"><span class="anchorjs-icon"></span></a></h1>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6 grid_box1">
                                                            <input type="text" class="form-control1" name="texto_1" value="<?php echo $texto_1;?>">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control1" name="texto_2" value="<?php echo $texto_2;?>">
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6 grid_box1">
                                                            <input type="text" class="form-control1" name="texto_3" value="<?php echo $texto_3;?>">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control1" name="texto_4" value="<?php echo $texto_4;?>">
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                              </td>                                  
                                            </tr>
                                            <tr>
                                              <td><h1 id="h1.-bootstrap-heading">Texto de pagina del blog<a class="anchorjs-link" href="#h1.-bootstrap-heading"><span class="anchorjs-icon"></span></a></h1>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6 grid_box1">
                                                            <input type="text" class="form-control1" name="texto_5" value="<?php echo $texto_5;?>">
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                              </td>                                  
                                            </tr>
                                            <tr>
                                              <td><h1 id="h1.-bootstrap-heading">Texto de página de audios<a class="anchorjs-link" href="#h1.-bootstrap-heading"><span class="anchorjs-icon"></span></a></h1>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6 grid_box1">
                                                            <input type="text" class="form-control1" name="texto_6" value="<?php echo $texto_6;?>">
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                              </td>                                  
                                            </tr>
                                            <tr>
                                              <td><h1 id="h1.-bootstrap-heading">Texto de página de contacto<a class="anchorjs-link" href="#h1.-bootstrap-heading"><span class="anchorjs-icon"></span></a></h1>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6 grid_box1">
                                                            <input type="text" class="form-control1" name="texto_7" value="<?php echo $texto_7;?>">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control1" name="texto_8" value="<?php echo $texto_8;?>">
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                              </td>        
                                            </tr>
                                            <tr>
                                              <td><h1 id="h1.-bootstrap-heading">Otros títulos<a class="anchorjs-link" href="#h1.-bootstrap-heading"><span class="anchorjs-icon"></span></a></h1>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6 grid_box1">
                                                            <input type="text" class="form-control1" name="texto_9" value="<?php echo $texto_9;?>">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control1" name="texto_10" value="<?php echo $texto_10;?>">
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6 grid_box1">
                                                            <input type="text" class="form-control1" name="texto_11" value="<?php echo $texto_11;?>">
                                                        </div>                                                
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                              </td>        
                                            </tr>
                                          </tbody>
                                         </table>
                                     </div>
                                  </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="hidden" name="otras_opciones" value="1"/>
                                                <input type="hidden" name="texto" value="1"/>
                                                <input type="submit" value="Subir cambios">
                                            </div>
                                            <div class="clearfix"> </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <?php
                            }    
                            ?>
                            
                            
                            
                            <div class="clearfix"></div>
                            <!-- //blog -->
                        </div>
                        <!--<div class="clearfix"></div>
                            <div class="fb-comments" data-href="http://localhost/radio/nuevo_blog.php?id=Programa_1_12_2018" data-numposts="5"></div>-->
                        <div class="clearfix"></div>
						<!--body wrapper end-->
            <!-- /w3l-agile -->
					</div>
			  <!--body wrapper end-->
			     <?php
                 require_once('pie.php');
                 ?>
		</div>
		<!-- /wthree-agile -->
        <!--footer section start-->
			<!--<footer>
			   <p>&copy 2016 Mosaic. All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">w3layouts.</a></p>
			</footer>-->
        <!--footer section end-->

      <!-- main content end-->
   </section>
  
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.js"></script>
</body>
</html>
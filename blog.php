<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Blog | Con M de memoria</title>
<?php
require_once('meta.php');
require_once('connection.php');
?>
        <!-- You can use open graph tags to customize link previews.-->
		
		<meta property="og:url"           content="http://localhost/radio/blog.php" />
		<meta property="og:type"          content="website" />
		<meta property="og:title"         content="Con M de memoria" />
		<meta property="og:image"         content="images/con-m-de-memoria.jpg"/>
		<meta property="og:description" content="Blog de Con M de memoria" />
</head> 
   <!-- /w3layouts -->
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
      <!-- left side start-->
		<?php
        //require_once('funciones.php');
        require_once('menu.php');
        ?>
		<!-- left side end-->
		 <!-- /agileinfo -->
					<!-- app-->
			<?php
            require_once('app.php');
            ?>
			<!-- //app-->
			 <!-- /agile-its -->
		<!-- signup -->
			<?php
            //require_once('registro.php');
            ?>
			<!-- //signup -->
	 <!-- /w3layouts-agile -->
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php
            require_once('cabecera.php');
            $sql=" select texto_5 from otros_datos where id=1";
            $res = mysql_query($sql);
            $res2 = mysql_fetch_array($res);
            $texto_5=$res2['texto_5'];
            ?>
					 <!-- /w3l-agile -->
				<!--notification menu end -->
				<!-- //header-ends -->
							<div id="page-wrapper">
								<div class="inner-content">
									<!-- /blog -->
									
										<div class="tittle-head">
											<h3 class="tittle"><?php echo $texto_5;?></h3>
											<div class="clearfix"> </div>
										</div>
										<!-- /music-left -->
										<div class="music-left">
                                            <?php
                                            $cont = 0;
                                            $sql=" select * from post where borrado=0 order by id desc"; 
                                            $datos=mysql_query($sql,$dbh); 
                                            while (($row=mysql_fetch_array($datos)) and ($cont<=2)){
                                                $id_post=$row['id'];
                                                $id_nombre_post=$row['nombre']." ".$row['mes']." ".$row['anio'];
                                                $id_nombre_post = str_replace(" ", "_",$id_nombre_post);//Reemplaza espacios en blanco por _
                                                ?>
                                                <div class="post-media">
                                                    <a href="nuevo_blog.php?id=<?php echo $id_nombre_post;?>"><img src="<?php echo $row['direccion_imagen'];?>" class="img-responsive" alt="<?php echo $row['imagen'];?>" /></a>
                                                    <div class="blog-text">
                                                        <a href="nuevo_blog.php?id=<?php echo $id_nombre_post;?>"><h3 class="h-t"><?php echo $row['nombre'];?></h3></a>
                                                        <div class="entry-meta">
                                                            <h6 class="blg"><i class="fa fa-clock-o"></i><?php echo $row['dia']."-".$row['mes']."-".$row['anio'];?> <!--Jan 25, 2016--></h6>
                                                            <div class="icons">
                                                                
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <p><?php echo $row['parrafo_1'];?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                $cont++;
                                            }
                                            mysql_free_result($datos);
                                            ?>
                                            <!--start-blog-pagenate-->
												<!--<div class="blog-pagenat">
													<ul>
														<li><a class="frist" href="#">Prev</a></li>
														<li><a href="#">1</a></li>
														<li><a href="#">2</a></li>
														<li><a href="#">3</a></li>
														<li><a href="#">4</a></li>
														<li><a href="#">5</a></li>
														<li><a class="last" href="#">Next</a></li>
														<div class="clearfix"> </div>
													</ul>
												</div>-->
											<!--//end-blog-pagenate-->

										</div>
										<!-- //music-left-->
										<!-- /music-right-->
										    
                                        <?php
                                        require_once('lateral_derecho.php');
                                        ?>    
                                        
										<div class="clearfix"></div>
									<!-- //blog -->
								</div>
							<div class="clearfix"></div>
						<!--body wrapper end-->
	 <!-- /w3l-agile -->
					</div>
			  <!--body wrapper end-->
			     <?php
                 require_once('pie.php');
                 ?>
			</div>
		<!-- /wthree-agile -->
        <!--footer section start-->
			<!--<footer>
			   <p>&copy 2016 Mosaic. All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">w3layouts.</a></p>
			</footer>-->
        <!--footer section end-->

      <!-- main content end-->
   </section>
  
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.js"></script>
</body>
</html>
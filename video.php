<!DOCTYPE HTML>
<html>
<head>
<title>Mosaic a Entertainment Category Flat Bootstrap Responsive Website Template | Typography :: w3layouts</title>
<?php
require_once('connection.php');
require_once('meta.php');
?>
</head> 
<body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
    <?php
    require_once('menu.php');
    require_once('app.php');
    require_once('registro.php');
	?>
        <div class="main-content">
        <?php
            require_once('cabecera.php');
        ?>
            <div id="page-wrapper">
                <div class="inner-content">
                    <div class="tittle-head">
                        <h3 class="tittle">Opciones</h3>
                        <div class="clearfix"> </div>
                    </div>
                    
                    <?php
                    require_once('opciones.php');
                    ?>
                        
                    <section id="tables">
						<div class="page-header">
							<a name="radio"><h1>Videos</h1></a>
						</div>
						<div class="bs-docs-example">
							<table class="table">
								<thead>
									<tr>
                                        <th>Nombre</th>
										<th>Código</th>
                                        <th>Lista</th>
										<th>Modificar</th>
										<th>Borrar</th>
									</tr>
								</thead>
								<tbody>
									<?php
                                    $sql=" select * from video where borrado=0"; 
									$datos=mysql_query($sql,$dbh);
									while ($row=mysql_fetch_array($datos)) {
                                        $id_programa=$row['id'];
                                        ?>
                                        <tr>
                                        <td>
                                        <a href="video.php?ver_video=1&id_video=<?php echo $row['id'];?>">
                                        <?php echo $row['nombre']?></a></td>
                                        <td><?php echo $row['codigo'];?></td>
                                        <?php 
                                        if ($row['tipo']=="1"){
                                            ?>
                                            <td>Video</td>
                                            <?php
                                        } else {
                                            ?>
                                            <td>Lista de reproducción</td>
                                            <?php    
                                        }
                                        ?>
                                        <td><a href="video.php?modificar_video=1&id_video=<?php echo $row['id'];?>#modificar_video">Modificar</a></td>
                                        <td><button onclick="myFunction_borrar(<?php echo $row['id'];?>)">Borrar</button></td>
                                        </tr>
                                        <?php
									}
									mysql_free_result($datos);?>
								</tbody>
							</table>
						</div>
                        <div class="in-right">
                            <form action="video.php" method="post">
                                <input type="submit" value="Agregar nuevo video">
                                <input type="hidden" name="nuevo_video" value="1">
                            </form>
                        </div>
                        <div class="clearfix"> </div>
					</section>
                    <script>
					function myFunction_borrar(id) {
						if (confirm("¿Está seguro de borrar?")) {
							window.location.replace('funciones.php?borrar_video=1&id_video='+id);
						} else {
							txt = "You pressed Cancel!";
						}
					}
					</script>
                    <?php
                    if (isset($_REQUEST['nuevo_video'])) {
                        $nuevo_video = $_REQUEST['nuevo_video'];
                    } else {
                        $nuevo_video = "";
                    }
                    if ($nuevo_video=="1"){
                        ?>
                        <form action="funciones.php" method="post">
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 grid_box1">
                                    <span class="input-group-addon" id="basic-addon2">Nombre</span>
                                    <input type="text" class="form-control1" name="nombre">
                                </div>
                                <div class="col-md-4 grid_box1">
                                    <span class="input-group-addon" id="basic-addon2">Código</span>
                                    <input type="text" class="form-control1" name="codigo">
                                </div>
                                <div class="col-md-4">
                                    <span class="input-group-addon" id="basic-addon2">Tipo de video</span>
                                    <select name="tipo">
                                      <option value="1">Video único</option>
                                      <option value="2">Lista de reproducción</option>
                                    </select> 
                                    
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="nuevo_video" value="1"/>
                                    <input type="submit" name ="enviar" value="Guardar_nuevo_video">
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        </form>
                        <?php    
                    }
                    if (isset($_REQUEST['modificar_video'])) {
						$modificar_video = $_REQUEST['modificar_video'];
					} else {
						$modificar_video = "";
					}
                    if ($modificar_video=="1"){
                        if (isset($_REQUEST['id_video'])) {
                            $id_video = $_REQUEST['id_video'];
                        } else {
                            $id_video = "";
                        }
                        $sql=" select * from video where id='$id_video'";
                        $res = mysql_query($sql);
                        $res2 = mysql_fetch_array($res);
                        ?>
                        <div class="typography">
                            <form action="funciones.php" method="post">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4 grid_box1">
                                        <span class="input-group-addon" id="basic-addon2">Nombre</span>
                                        <input type="text" class="form-control1" name="nombre" value="<?php echo $res2['nombre'];?>">
                                    </div>
                                    <div class="col-md-4 grid_box1">
                                        <span class="input-group-addon" id="basic-addon2">Código</span>
                                        <input type="text" class="form-control1" name="codigo" value="<?php echo $res2['codigo'];?>">
                                    </div>
                                    <div class="col-md-4">
                                        <span class="input-group-addon" id="basic-addon2">Tipo de video</span>
                                        <select name="tipo">
                                            <?php 
                                            if ($res2['tipo']=="1"){
                                                ?>
                                                <option value="1" select>Video único</option>
                                                <option value="2">Lista de reproducción</option>
                                                <?php    
                                            } else {
                                                ?>
                                                <option value="1">Video único</option>
                                                <option value="2" select>Lista de reproducción</option>
                                                <?php
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="modificar_video" value="1"/>
                                        <input type="hidden" name="id_video" value="<?php echo $res2['id'];?>"/>
                                        <input type="submit" name ="enviar" value="Guardar modificación">
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                            </form>
                        </div>
                        <?php
                    }
                    if (isset($_REQUEST['ver_video'])) {
                        $ver_video = $_REQUEST['ver_video'];
                    } else {
                        $ver_video = "";
                    }
                    if ($ver_video=="1"){
                        if (isset($_REQUEST['id_video'])) {
                            $id_video = $_REQUEST['id_video'];
                        } else {
                            $id_video = "";
                        }
                        $sql=" select * from video where id='$id_video'"; 
                        $datos=mysql_query($sql,$dbh);
                        while ($row=mysql_fetch_array($datos)) {
                            if ($row['tipo']=="1"){
                                ?>
                                <iframe width="560" height="315" src="<?php echo $row['codigo'];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <div class="clearfix"></div>
                                <?php
                            } else {
                                ?>
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=<?php echo $row['codigo'];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <div class="clearfix"></div>
                                <?php    
                            }
                        }
                        mysql_free_result($datos);
                    }
                    ?>
                </div>
            </div>    
        </div>
    </section>
</body>    
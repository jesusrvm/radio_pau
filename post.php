<!DOCTYPE HTML>
<html>
<head>
<title>Mosaic a Entertainment Category Flat Bootstrap Responsive Website Template | Typography :: w3layouts</title>
<?php
require_once('connection.php');
require_once('meta.php');
?>
</head> 
<body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
    <?php
    require_once('menu.php');
    require_once('app.php');
    require_once('registro.php');
	?>
	<div class="main-content">
	<?php
    require_once('cabecera.php');
    ?>
		<div id="page-wrapper">
			<div class="inner-content">
				<div class="tittle-head">
					<h3 class="tittle">Opciones</h3>
					<div class="clearfix"> </div>
				</div>
				<?php
                require_once('opciones.php');
                    ?>
					<section id="tables">
						<div class="page-header">
							<a name="radio"><h1>Listado de Posts</h1></a>
						</div>
						<div class="bs-docs-example">
							<table class="table">
								<thead>
									<tr>
										<th>Post #</th>
										<th>Nombre</th>
										<th>Imagen</th>
										<th>Modificar</th>
										<th>Borrar</th>
									</tr>
								</thead>
								<tbody>
									<?php
                                    $sql=" select id, nombre, imagen, numero from post where borrado=0 order by id desc"; 
									$datos=mysql_query($sql,$dbh); 
									while ($row=mysql_fetch_array($datos)) {
                                        $id_post=$row['id'];
                                        ?>
										<tr>
										<td><a href="post.php?ver_datos=1&id_post=<?php echo $id_post?>#ver_datos"><?php echo $row['numero']?></a></td>
										<td><a href="post.php?ver_datos=1&id_post=<?php echo $id_post;?>#ver_datos"><?php echo $row['nombre']?></a></td>
                                        <td><a href="post.php?ver_datos=1&id_post=<?php echo $id_post;?>#ver_datos"><?php echo $row['imagen']?></a></td>
                                        <td><a href="post.php?modificar_post=1&id_post=<?php echo $id_post;?>#modificar_post">Modificar</a></td>
										<td><button onclick="myFunction_borrar(<?php echo $id_post;?>)">Borrar</button></td>
										</tr>
										<?php
									}		
									mysql_free_result($datos);?>
								</tbody>
							</table>
						</div>
                        
                        
                        
					</section>
					
					<script>
					function myFunction_borrar(id) {
						if (confirm("¿Está seguro de borrar?")) {
							window.location.replace('funciones.php?borrar_post=1&id='+id);
						} else {
							txt = "You pressed Cancel!";
						}
					}
					</script>
					<?php
					if (isset($_REQUEST['nuevo_post'])) {
						$nuevo_post = $_REQUEST['nuevo_post'];
					} else {
						$nuevo_post = "";
					}
                    if (isset($_REQUEST['ver_datos'])) {
						$ver_datos = $_REQUEST['ver_datos'];
					} else {
						$ver_datos = "";
					}
					if (isset($_REQUEST['modificar_post'])) {
						$modificar_post = $_REQUEST['modificar_post'];
					} else {
						$modificar_post = "";
					}
					if ($nuevo_post=="1"){
						?>
						<a name="nuevo_programa">
						<form action="funciones.php" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<div class="row">
									<div class="col-md-2 grid_box1">
										<input type="text" class="form-control1" name="numero" placeholder="Número de post">
									</div>
									<div class="col-md-10">
										<input type="text" class="form-control1" name="nombre" placeholder="Nombre del post">
									</div>
									<div class="clearfix"> </div>
							</div>
						</div>
						</a>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<input type="text" class="form-control1" name="parrafo_1" placeholder="Párrafo 1">
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<input type="text" class="form-control1" name="parrafo_2" placeholder="Párrafo 2">
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<input type="text" class="form-control1" name="parrafo_3" placeholder="Párrafo 3">
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<input type="text" class="form-control1" name="codigo" placeholder="Código">
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6 grid_box1">
                                    <input type="text" class="form-control1" name="nombre_imagen" placeholder="Nombre de la imagen">
                                </div>
                                <div class="col-md-6">
                                    <input type="file" name="imagen"/>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="nuevo_post" value="1"/>
									<input type="submit" name ="enviar" value="Subir_post">
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        </form>
						<?php
					}
                    if ($ver_datos=="1"){
                        if (isset($_REQUEST['id_post'])) {
                            $id_post = $_REQUEST['id_post'];
                        } else {
                            $id_post = "";
                        }
                        $sql=" select * from post where id=$id_post";
                        $res = mysql_query($sql);
                        $res2 = mysql_fetch_array($res);
                        ?>
                        <a name="ver_datos">
						<div class="form-group">
							<div class="row">
                                <div class="col-md-2 grid_box1">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['numero'];?></span>
                                </div>
                                <div class="col-md-10">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['nombre'];?></span>
                                </div>
                                <div class="clearfix"> </div>
							</div>
						</div>
						</a>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['parrafo_1'];?></span>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['parrafo_2'];?></span>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['parrafo_3'];?></span>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2 grid_box1">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['dia']."/".$res2['mes']."/".$res2['anio'];?></span>
                                </div>
                                <div class="col-md-10">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['codigo'];?></span>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['imagen'];?></span>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <img class="media-object" src="<?php echo $res2['direccion_imagen'];?>" alt="<?php echo $res2['imagen'];?>">
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <?php
                    }
					if (isset($_REQUEST['modificar_post'])) {
						$modificar_post = $_REQUEST['modificar_post'];
					} else {
						$modificar_post = "";
					}
					if ($modificar_post=="1"){
						if (isset($_REQUEST['id_post'])) {
                            $id_post = $_REQUEST['id_post'];
                        } else {
                            $id_post = "";
                        }
                        $sql=" select * from post where id=$id_post";
                        $res = mysql_query($sql);
                        $res2 = mysql_fetch_array($res);
                        ?>
						<a name="nuevo_programa">
						<form action="funciones.php" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<div class="row">
                                <div class="col-md-2 grid_box1">
									<?php 
									if ($res2['numero']<>""){
										?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Nro. Post</span>
                                            <input type="text" class="form-control1" name="numero" value="<?php echo $res2['numero'];?>">
                                        </div>
                                        <?php
									} else {
										?>
										<input type="text" class="form-control1" name="numero" placeholder="Número de post">
										<?php
									}		
									?>
								</div>
								<div class="col-md-10">
                                    <?php
                                    if ($res2['nombre']<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Nombre</span>
                                            <input type="text" class="form-control1" name="nombre" value="<?php echo $res2['nombre'];?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="nombre" placeholder="Nombre del post">
                                       <?php
                                    }
                                    ?>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						</a>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
                                    <?php 
                                    if ($res2['parrafo_1']<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Descripción 1</span>
                                            <input type="text" class="form-control1" name="parrafo_1" value="<?php echo $res2['parrafo_1'];?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="parrafo_1" placeholder="Descripción 1">
                                        <?php    
                                    }
                                    ?>
                                </div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
                                    <?php
                                    if ($res2['parrafo_2']<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Descripción 2</span>
                                            <input type="text" class="form-control1" name="parrafo_2" value="<?php echo $res2['parrafo_2'];?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="parrafo_2" placeholder="Descripción 2">
                                        <?php    
                                    }
                                    ?>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
                                    <?php
                                    if ($res2['parrafo_3']<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Descripción 3</span>
                                            <input type="text" class="form-control1" name="parrafo_3" value="<?php echo $res2['parrafo_3'];?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="parrafo_3" placeholder="Descripción 3">
                                        <?php    
                                    }
                                    ?>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
                                    <?php
                                    if ($res2['codigo']<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Código</span>
                                            <input type="text" class="form-control1" name="codigo" value="<?php echo $res2['codigo'];?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="codigo" placeholder="Código">
                                        <?php    
                                    }
                                    ?>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    if ($res2['imagen']<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Nombre de la imagen</span>
                                            <input type="text" class="form-control1" name="nombre_imagen" value="<?php echo $res2['imagen'];?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="nombre_imagen" placeholder="Nombre de la imagen">
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    if ($res2['direccion_imagen']<>""){
                                        ?>
                                        <img class="media-object" src="<?php echo $res2['direccion_imagen'];?>" alt="<?php echo $res2['imagen'];?>"><br>
                                        <input type="file" name="imagen"/>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="col-md-3 grid_box1">
                                            <input type="file" name="imagen"/>
                                        </div>
                                        <?php    
                                    }
                                    ?>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="modificar_post" value="1"/>
                                    <input type="hidden" name="id_post" value="<?php echo $res2['id'];?>"/>
									<input type="submit" name ="enviar" value="Subir_cambios">
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        </form>
						<?php
					}

                    if (($nuevo_post=="") and ($modificar_post=="") and ($ver_datos=="")){
                        ?>
                        <div class="in-right">
                            <form action="post.php#nuevo_post" method="post">
                                <input type="submit" value="Agregar nuevo post">
                                <input type="hidden" name="nuevo_post" value="1">
                            </form>
                        </div>
                        <?php
                    }
                    ?>
					
				
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<?php
	require_once('pie.php');
	?>
</section>
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<script src="js/bootstrap.js"></script>
</body>
</html>
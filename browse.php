
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Mosaic a Entertainment Category Flat Bootstrap Responsive Website Template | Browse :: w3layouts</title>
<?php
require_once('meta.php');
?>
</head> 
    	 <!-- /w3layouts-agile -->
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
      <!-- left side start-->
		<?php
        require_once('menu.php');
        ?>
		</div>
		<!-- left side end-->
					<!-- app-->
			<?php
            require_once('app.php');
            ?>
			<!-- //app-->
			 	 <!-- /w3l-agile -->
		<!-- signup -->
			<?php
            require_once('registro.php');
            ?>
			<!-- //signup -->
 	 <!-- /agileits -->
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php
            require_once('cabecera.php');
            ?>
			<!--notification menu end -->
			<!-- //header-ends -->
 	 <!-- /agileinfo -->
		<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="inner-content">
				      <div class="music-browse">
					<!--albums-->
					<!-- pop-up-box --> 
							<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all">
							<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
							 <script>
									$(document).ready(function() {
									$('.popup-with-zoom-anim').magnificPopup({
										type: 'inline',
										fixedContentPos: false,
										fixedBgPos: true,
										overflowY: 'auto',
										closeBtnInside: true,
										preloader: false,
										midClick: true,
										removalDelay: 300,
										mainClass: 'my-mfp-zoom-in'
									});
									});
							</script>		
					<!--//pop-up-box -->
					
						<div class="browse">
								<div class="tittle-head two">
									<h3 class="tittle">New Releses <span class="new">New</span></h3>
									<a href="browse.php"><h4 class="tittle third">See all</h4></a>
									<div class="clearfix"> </div>
								</div>

								<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v11.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
									<a class="sing" href="single.php">Lootera</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v22.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Jaremy Cam</a>
								</div>
							<div class="col-md-3 browse-grid">
								<a  href="single.php"><img src="images/v33.jpg" title="allbum-name"></a>
								 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Selah</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v44.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Jim Brickman</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v1.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Adele21</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v55.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Party Night</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v6.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Ellie Goluding</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v66.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
									<a class="sing" href="single.php">Diana</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v6.jpeg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Fifty Shades</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v2.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Shomlock</a>
								</div>
								<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v3.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Lootera</a>
								</div>
								<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v4.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Stuck on a feeling</a>
								</div>
											<div class="clearfix"> </div>
									</div>
					<!--//End-albums-->
					
					<div class="browse">
								<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v10.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
									<a class="sing" href="single.php">Fifty Shades</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v9.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Alan Jackson</a>
								</div>
							<div class="col-md-3 browse-grid">
								<a  href="single.php"><img src="images/v77.jpg" title="allbum-name"></a>
								 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Cheristina aguilera</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v88.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Samsmith</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v1.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Adele21</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v99.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Big Duty</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v6.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Ellie Goluding</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v66.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
									<a class="sing" href="single.php">Diana</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v6.jpeg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Fifty Shades</a>
								</div>
							<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v21.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Joe</a>
								</div>
								<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v3.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Lootera</a>
								</div>
								<div class="col-md-3 browse-grid">
									<a  href="single.php"><img src="images/v4.jpg" title="allbum-name"></a>
									 <a href="single.php"><i class="glyphicon glyphicon-play-circle"></i></a>
										<a class="sing" href="single.php">Stuck on a feeling</a>
								</div>
											<div class="clearfix"> </div>
									</div>
					<!--//End-albums-->
						<!--//discover-view-->
							<!--//music-left-->
							</div>
							
						<!--body wrapper start-->
						<div class="review-slider">
								<div class="tittle-head">
									<h3 class="tittle">Featured Albums <span class="new"> New</span></h3>
									<div class="clearfix"> </div>
								</div>
								 <ul id="flexiselDemo1">
								<li>
									<a href="single.php"><img src="images/v1.jpg" alt=""/></a>
									<div class="slide-title"><h4>Adele21 </div>
									<div class="date-city">
										<h5>Jan-02-16</h5>
										<div class="buy-tickets">
											<a href="single.php">READ MORE</a>
										</div>
									</div>
								</li>
								<li>
									<a href="single.php"><img src="images/v2.jpg" alt=""/></a>
									<div class="slide-title"><h4>Adele21</h4></div>
									<div class="date-city">
										<h5>Jan-02-16</h5>
										<div class="buy-tickets">
											<a href="single.php">READ MORE</a>
										</div>
									</div>
								</li>
								<li>
									<a href="single.php"><img src="images/v21.jpg" alt=""/></a>
									<div class="slide-title"><h4>Joe</h4></div>
									<div class="date-city">
										<h5>Jan-02-16</h5>
										<div class="buy-tickets">
											<a href="single.php">READ MORE</a>
										</div>
									</div>
								</li>
								<li>
									<a href="single.php"><img src="images/v4.jpg" alt=""/></a>
									<div class="slide-title"><h4>Stuck on a feeling</h4></div>
									<div class="date-city">
										<h5>Jan-02-16</h5>
										<div class="buy-tickets">
											<a href="single.php">READ MORE</a>
										</div>
									</div>
								</li>
								<li>
									<a href="single.php"><img src="images/v5.jpg" alt=""/></a>
									<div class="slide-title"><h4>Ricky Martine </h4></div>
									<div class="date-city">
										<h5>Jan-02-16</h5>
										<div class="buy-tickets">
											<a href="single.php">READ MORE</a>
										</div>
									</div>
								</li>
								<li>
									<a href="single.php"><img src="images/v6.jpg" alt=""/></a>
									<div class="slide-title"><h4>Ellie Goluding</h4></div>
									<div class="date-city">
										<h5>Jan-02-16</h5>
										<div class="buy-tickets">
											<a href="single.php">READ MORE</a>
										</div>
									</div>
								</li>
								<li>
									<a href="single.php"><img src="images/v6.jpeg" alt=""/></a>
									<div class="slide-title"><h4>Fifty Shades </h4></div>
									<div class="date-city">
										<h5>Jan-02-16</h5>
										<div class="buy-tickets">
											<a href="single.php">READ MORE</a>
										</div>
									</div>
								</li>
							</ul>
							<script type="text/javascript">
						$(window).load(function() {
							
						  $("#flexiselDemo1").flexisel({
								visibleItems: 5,
								animationSpeed: 1000,
								autoPlay: true,
								autoPlaySpeed: 3000,    		
								pauseOnHover: false,
								enableResponsiveBreakpoints: true,
								responsiveBreakpoints: { 
									portrait: { 
										changePoint:480,
										visibleItems: 2
									}, 
									landscape: { 
										changePoint:640,
										visibleItems: 3
									},
									tablet: { 
										changePoint:800,
										visibleItems: 4
									}
								}
							});
							});
						</script>
						<script type="text/javascript" src="js/jquery.flexisel.js"></script>	
						</div>
								</div>
							<div class="clearfix"></div>
						<!--body wrapper end-->
 	 <!-- /w3l-agile-info -->
					</div>
			  <!--body wrapper end-->
			     <?php
                 require_once('pie.php');
                 ?>
			</div>
        <!--footer section start-->
			<!--<footer>
			   <p>&copy 2016 Mosaic. All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">w3layouts.</a></p>
			</footer>-->
        <!--footer section end-->
 	 <!-- /wthree-agile -->
      <!-- main content end-->
   </section>
   	 <!-- /wthree-agile -->
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.js"></script>
</body>
</html>
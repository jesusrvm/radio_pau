<html>
<head>
<style>
 /* Para tener un ancho máximo debemos de establecer el width del DIV.delimitador */
.delimitador{

    width:560px;
    margin:auto;

}
/* El contenedor con el padding-top crea el tamaño del vídeo */
.contenedor{

    height:0px;
    width:100%;
    max-width:560px; /* Así establecemos el ancho máximo (si lo queremos) */
    padding-top:56.25%; /* Relación: 16/9 = 56.25% */
    position:relative;

}
/* El iframe se adapta al tamaño del contenedor */
iframe{

    position:absolute;
    height:100%;
    width:100%;
    top:0px;
    left:0px;

}

.video-responsive {
position: relative;
padding-bottom: 56.25%; /* 16/9 ratio */
padding-top: 30px; /* IE6 workaround*/
height: 0;
overflow: hidden;
}

.video-responsive iframe,
.video-responsive object,
.video-responsive embed {
position: absolute;
top: 0;
left: 0;
width: 100%;
height: 100%;
}

.video-container {

padding-bottom:56.25%;
height:0; overflow: hidden;
position: relative;
}
 

</style>
</head>
<body>
<div class="delimitador">

    <div class="contenedor">

        <iframe src="http://www.youtube.com/embed/wMhL_QIyD1k?rel=0"></iframe>

    </div>
    <div class="contenedor">

        <iframe src="http://player.vimeo.com/video/52553020"></iframe>

    </div>

</div>
    
<!--<div class="”video-responsive”">
<iframe  src="https://www.youtube.com/embed/sOnqjkJTMaA" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</div>-->

<div class="video-container">
<iframe width="320" height="240" src="//www.youtube.com/embed/FOIQb78hfi8"></iframe>
</div>
</body>
</html>
<?php 
header("Content-type: text/css");

require_once('../connection.php');
$sql="select color_1, color_2 from otros_datos where id=1";
$datos=mysql_query($sql,$dbh);
while ($row=mysql_fetch_array($datos)) {
    $color_1 = $row['color_1'];
    $color_2 = $row['color_2'];
}
mysql_free_result($datos);
?>

.custom-nav > li > a:hover,
.custom-nav > li > a:active {
    color:<?php echo $color_1;?>;
}

.custom-nav > li.nav-active > a {
    color:<?php echo $color_1;?>;
}

.custom-nav > li.active > a,
.custom-nav > li.active > a:hover,
.custom-nav > li.active > a:focus {
    color:<?php echo $color_1;?>;
}

.left-side-collapsed .custom-nav li a span {
    background:<?php echo $color_1;?>;
}

.left-side-collapsed .custom-nav li a span:after {
    border-right-color:<?php echo $color_1;?>;
}

.left-side-collapsed .custom-nav > li.nav-hover > a,
.left-side-collapsed .custom-nav > li.nav-hover.active > a {
    color:<?php echo $color_1;?>;
}

.left-side-collapsed .custom-nav li.nav-hover.active a span {
    background:<?php echo $color_1;?>;
}

.toggle-btn:hover {
    background:<?php echo $color_1;?>;
}

.sticky-header .logo {
    background: <?php echo $color_1;?>;
}

footer {
    background:<?php echo $color_1;?>;
}

.callbacks_tabs a:after {
    background:<?php echo $color_1;?>;
}

a.trend {
    background: <?php echo $color_1;?>;
}

#loginpop a span{
    color: <?php echo $color_1;?>;
}

#loginForm #login:hover{
     background:<?php echo $color_1;?>;
}

.button:hover {
    color:<?php echo $color_1;?>;
}

.content-grid:hover .inner-info{ 
    background: <?php echo $color_1;?>; 
}

h4.tittle {    
    color: <?php echo $color_1;?>;
}

span.new{    
    background: <?php echo $color_1;?>;
}

.price-top {    
    border: 1px solid <?php echo $color_1;?>;
}

a.price {
    background: <?php echo $color_1;?>;
}

.price-bottom {
	border: 1px solid <?php echo $color_1;?>;
}

i.glyphicon.glyphicon-ok {
    color: <?php echo $color_1;?>;
}

.media-body.response-text-right ul li {
	color:<?php echo $color_1;?>;    
}

.media-body.response-text-right ul li a:hover {
	color:<?php echo $color_1;?>;
}

.coment-form input[type="submit"]:hover{
    background:<?php echo $color_1;?>;
}

.search_footer input[type="submit"]:hover{
	background:<?php echo $color_1;?>;
}

p.email{
	color: <?php echo $color_1;?>;
}

p.email span a{
	color:<?php echo $color_1;?>;
}

.date-city h5 {
	color:<?php echo $color_1;?>;
}

.buy-tickets a:hover {
	background-color:<?php echo $color_1;?>;
}

a.sing:hover{
    color:<?php echo $color_1;?>;
}

a.art:hover{
    color:<?php echo $color_1;?>;
}

.sign-right h3{
    color: <?php echo $color_1;?>;
}

.sign-right input[type="submit"]:hover {
	background:<?php echo $color_1;?>;
}

.sign-grids p span{
	color:<?php echo $color_1;?>;
}

.app-left h3 {
    color: <?php echo $color_1;?>;
}

.app-devices h5 {
	color: <?php echo $color_1;?>;
}

.dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover {
    background-color: <?php echo $color_1;?>;
}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
	background-color: <?php echo $color_1;?>;
}

.nav-tabs > li > a:hover, .nav-tabs > li > a:focus {
	background-color: <?php echo $color_1;?>;
}

.blog-text a:hover,h3.h-t:hover,.widget-side ul li a:hover{
 color:<?php echo $color_1;?>; 
}

.blog-pagenat ul li a:hover {
    background:<?php echo $color_1;?>;
    border: 1px solid <?php echo $color_1;?>;
}

.contact-right p.phn{
	color: <?php echo $color_1;?>;
}

.in-right input[type="submit"]:hover{
	background:<?php echo $color_1;?>;
}

.contact-right1 h4{
    color: <?php echo $color_1;?>;
}

a.not {    
    background: <?php echo $color_1;?>;
}

i.glyphicon.glyphicon-ok {
	color: <?php echo $color_1;?>;
}

.custom-nav .sub-menu-list > li > a:hover,
.custom-nav .sub-menu-list > li > a:active,
.custom-nav .sub-menu-list > li > a:focus {    
    background:<?php echo $color_2;?>;
}

.left-side-collapsed .custom-nav li.nav-hover ul {    
    background:<?php echo $color_2;?>;
}

.header-section {
    background:<?php echo $color_2;?>;
}

.callbacks_here a:after{
  background:<?php echo $color_2;?>;
}

.banner-info p span {
 color:<?php echo $color_2;?>;
}

a.trend:hover {
  background:<?php echo $color_2;?>;
}

#loginForm #login {
  background:<?php echo $color_2;?>;
}

div.jp-type-playlist div.jp-playlist a:hover {
	background: <?php echo $color_2;?>!important;
}

.pricing:hover a.price {
	background:<?php echo $color_2;?>;
}

.coment-form input[type="submit"] {
  background:<?php echo $color_2;?>;
}

.buy-tickets a {
	background: <?php echo $color_2;?>;
}
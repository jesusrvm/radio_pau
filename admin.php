<!DOCTYPE HTML>
<html>
<head>
<title>Mosaic a Entertainment Category Flat Bootstrap Responsive Website Template | Typography :: w3layouts</title>
<?php
require_once('connection.php');
require_once('meta.php');
?>
</head> 
<body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
    <?php
    require_once('menu.php');
    require_once('app.php');
    require_once('registro.php');
	
	if (isset($_REQUEST['radio'])) {
		$radio = $_REQUEST['radio'];
	} else {
		$radio = "";
	}
	if (isset($_REQUEST['video'])) {
		$video = $_REQUEST['video'];
	} else {
		$video = "";
	}
	if (isset($_REQUEST['post'])) {
		$post = $_REQUEST['post'];
	} else {
		$post = "";
	}
    ?>
	<div class="main-content">
	<?php
    require_once('cabecera.php');
    ?>
		<div id="page-wrapper">
			<div class="inner-content">
				<div class="tittle-head">
					<h3 class="tittle">Opciones</h3>
					<div class="clearfix"> </div>
				</div>
				<?php
                require_once('opciones.php');
				if ($radio=="1"){
					?>
					<section id="tables">
						<div class="page-header">
							<a name="radio"><h1>Audios</h1></a>
						</div>
						<div class="bs-docs-example">
							<table class="table">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Predeterminar</th>
										<th>Modificar</th>
										<th>Borrar</th>
									</tr>
								</thead>
								<tbody>
									<?php
                                    $ban=0;
									$sql=" select id, nombre, predeterminado from audio where (borrado=0)"; 
									$datos=mysql_query($sql,$dbh); 
									while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
                                        $id_audio = $row['id'];
										if ($row['predeterminado']=="1"){
											?>
											<tr  bgcolor="yellow">
											<td><a href="admin.php?ver_datos=1&=1&id_audio=<?php echo $id_audio;?>#ver_datos"><?php echo $row['nombre']?></a></td>
											<td><a href="funciones.php?predeterminar_audio=1&id_audio=<?php echo $id_audio;?>">Predeterminar</a></td>
											<td><a href="admin.php?modificar_audio=1&audio=1&id_audio=<?php echo $id_audio;?>#modificar_audio">Modificar</a></td>
											<td><button onclick="myFunction_borrar(<?php echo $id_audio;?>)">Borrar</button></td>
											</tr>
											<?php
										} else {
											?>
											<tr>
											<td><a href="admin.php?ver_datos=1&audio=1&id_audio=<?php echo $id_audio;?>#ver_datos"><?php echo $row['nombre']?></a></td>
											<td><a href="funciones.php?predeterminar_audio=1&id_audio=<?php echo $id_audio;?>">Predeterminar</a></td>
											<td><a href="admin.php?modificar_audio=1&radio=1&id_audio=<?php echo $id_audio;?>#modificar_audio">Modificar</a></td>
											<td><button onclick="myFunction_borrar(<?php echo $id_audio;?>)">Borrar</button></td>
											</tr>
											<?php
										}		
									}
									mysql_free_result($datos);?>
								</tbody>
							</table>
						</div>
					</section>
					
					<script>
					function myFunction_borrar(id) {
						if (confirm("¿Está seguro de borrar?")) {
							window.location.replace('funciones.php?borrar_audio=1&id='+id);
						} else {
							txt = "You pressed Cancel!";
						}
					}
					</script>
					
					<?php
					if (isset($_REQUEST['nuevo_audio'])) {
						$nuevo_audio = $_REQUEST['nuevo_audio'];
					} else {
						$nuevo_audio = "";
					}
                    if (isset($_REQUEST['ver_datos'])) {
						$ver_datos = $_REQUEST['ver_datos'];
					} else {
						$ver_datos = "";
					}
					if (isset($_REQUEST['modificar_audio'])) {
						$modificar_audio = $_REQUEST['modificar_audio'];
					} else {
						$modificar_audio = "";
					}
					if ($nuevo_audio=="1"){
						?>
						<a name="nuevo_audio">
						<form action="funciones.php" method="post" enctype="multipart/form-data">
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-3 grid_box1">
                                    <input type="text" class="form-control1" name="nombre_imagen" placeholder="Nombre de la imagen">
                                </div>
                                <div class="col-md-3 grid_box1">
                                    <input type="file" name="imagen"/>
                                </div>
                                <div class="col-md-3 grid_box1">
                                    <input type="text" class="form-control1" name="nombre_audio" placeholder="Nombre del audio">
                                </div>
                                <div class="col-md-3">
                                    <input type="file" name="audio"/>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        </a>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="nuevo_audio" value="1"/>
									<input type="submit" name ="enviar" value="Subir_audio">
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        </form>
						<?php
					}
                    if ($ver_datos=="1"){
                        if (isset($_REQUEST['id_audio'])) {
                            $id_audio = $_REQUEST['id_audio'];
                        } else {
                            $id_audio = "";
                        }
                        $sql=" select * from audio where id=$id_audio";
                        $res = mysql_query($sql);
                        $res2 = mysql_fetch_array($res);
                        $nombre_audio=$res2['nombre_audio'];
                        $direccion_audio=$res2['direccion_audio'];
                        $nombre_imagen=$res2['nombre_imagen'];
                        $direccion_imagen=$res2['direccion_imagen'];
                        ?>
                        <a name="ver_datos">
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6 grid_box1">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $nombre_imagen;?></span>
                                </div>
                                <div class="col-md-6">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $nombre_audio;?></span>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        </a>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6 grid_box1">
                                    <img class="media-object" src="<?php echo $direccion_imagen;?>" alt="<?php echo $nombre_imagen;?>">
                                </div>
                                <div class="col-md-6">
                                    <audio id="sonido" src="<?php echo $direccion_audio;?>" controls></audio>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <?php
                    }
					if (isset($_REQUEST['modificar_audio'])) {
						$modificar_audio = $_REQUEST['modificar_audio'];
					} else {
						$modificar_audio = "";
					}
					if ($modificar_audio == "1"){
						if (isset($_REQUEST['id_audio'])) {
                            $id_audio = $_REQUEST['id_audio'];
                        } else {
                            $id_audio = "";
                        }
                        $sql=" select * from audio where id=$id_audio";
                        $res = mysql_query($sql);
                        $res2 = mysql_fetch_array($res);
                        $nombre_audio=$res2['nombre_audio'];
                        $direccion_audio=$res2['direccion_audio'];
                        $nombre_imagen=$res2['nombre_imagen'];
                        $direccion_imagen=$res2['direccion_imagen'];
                        ?>
						<a name="nuevo_audio">
						<form action="funciones.php" method="post" enctype="multipart/form-data">
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6 grid_box1">
                                    <?php
                                    if ($nombre_imagen<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Nombre de la imagen</span>
                                            <input type="text" class="form-control1" name="nombre_imagen" value="<?php echo $nombre_imagen;?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="nombre_imagen" placeholder="Nombre de la imagen">
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <?php
                                    if ($nombre_audio<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Nombre del audio</span>
                                            <input type="text" class="form-control1" name="nombre_audio" value="<?php echo $nombre_audio;?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="nombre_audio" placeholder="Nombre del audio">
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        </a>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6 grid_box1">
                                    <?php
                                    if ($direccion_imagen<>""){
                                        ?>
                                        <img class="media-object" src="<?php echo $direccion_imagen;?>" alt="<?php echo $nombre_imagen;?>">
                                        <input type="file" name="imagen"/>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="col-md-3 grid_box1">
                                            <input type="file" name="imagen"/>
                                        </div>
                                        <?php    
                                    }
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <?php
                                    if ($direccion_audio<>""){
                                        ?>
                                        <audio id="sonido" src="<?php echo $direccion_audio;?>" controls></audio>
                                        <input type="file" name="audio"/>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="col-md-3">
                                            <input type="file" name="audio"/>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        </form>
						<?php
					}	
					?>
					<div class="in-right">
						<form action="admin.php#nuevo_audio" method="post">
							<input type="submit" value="Agregar nuevo audio">
							<input type="hidden" name="audio" value="1">
							<input type="hidden" name="nuevo_audio" value="1">
						</form>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<?php
	require_once('pie.php');
	?>
</section>
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<script src="js/bootstrap.js"></script>
</body>
</html>
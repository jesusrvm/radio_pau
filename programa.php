<!DOCTYPE HTML>
<html>
<head>
<title>Programas | Con M de memoria</title>
<?php
require_once('meta.php');
?>
</head> 
<body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
    <?php
    require_once('connection.php');
    require_once('menu.php');
    require_once('app.php');
    //require_once('registro.php');
	?>
	<div class="main-content">
	<?php
    require_once('cabecera.php');
    ?>
		<div id="page-wrapper">
			<div class="inner-content">
				<div class="tittle-head">
					<h3 class="tittle">Opciones</h3>
					<div class="clearfix"> </div>
				</div>
				<?php
                require_once('opciones.php');
                               
                    ?>
                    <!-- pop-up-box --> 
							<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all">
							<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
							 <script>
									$(document).ready(function() {
									$('.popup-with-zoom-anim').magnificPopup({
										type: 'inline',
										fixedContentPos: false,
										fixedBgPos: true,
										overflowY: 'auto',
										closeBtnInside: true,
										preloader: false,
										midClick: true,
										removalDelay: 300,
										mainClass: 'my-mfp-zoom-in'
									});
									});
							</script>		
					<!--//pop-up-box -->
					<section id="tables">
						<div class="page-header">
							<a name="radio"><h1>Listado de Programas</h1></a>
						</div>
						<div class="bs-docs-example">
							<table class="table">
								<thead>
									<tr>
										<th>Programa #</th>
										<th>Nombre</th>
                                        <th>Enlace</th>
										<th>Imagen</th>
										<th>Modificar</th>
										<th>Borrar</th>
									</tr>
								</thead>
								<tbody>
									<?php
                                    $cont = 0;
                                    $sql=" select id, nombre, nombre_imagen, enlace, numero from radio where borrado=0 order by id desc"; 
									$datos=mysql_query($sql,$dbh); 
									while ($row=mysql_fetch_array($datos)) {
                                        $cont++;
                                        ?>
										<tr>
										<td>
                                        <a href="programa.php?ver_datos=1&id_programa=<?php echo $row['id'];?>#ver_datos"><?php echo $row['numero']?></a></td>
										<td><a href="programa.php?ver_datos=1&id_programa=<?php echo $row['id'];?>#ver_datos"><?php echo $row['nombre']?></a>
                                        </td>
                                        <td>
                                        <a class="button play-icon popup-with-zoom-anim" href="<?php echo "#small-dialog".$cont;?>">Escuchar ahora</a>
                                        <div id="<?php echo "small-dialog".$cont;?>" class="mfp-hide">HOLA<?php echo $row['enlace']?></div>
                                        </td>
                                        <td>
                                        <a href="programa.php?ver_datos=1&id_programa=<?php echo $row['id'];?>#ver_datos"><?php echo $row['nombre_imagen']?></a>
                                        </td>
                                        <td>
                                        <a href="programa.php?modificar_programa=1&id_programa=<?php echo $row['id'];?>#modificar_post">Modificar</a></td>
										<td><button onclick="myFunction_borrar(<?php echo $row['id'];?>)">Borrar</button>
                                        </td>
										</tr>
										<?php
									}		
									mysql_free_result($datos);?>
								</tbody>
							</table>
						</div>
                    </section>
					
					<script>
					function myFunction_borrar(id) {
						if (confirm("¿Está seguro de borrar?")) {
							window.location.replace('funciones.php?borrar_programa_radio=1&id='+id);
						} else {
							txt = "You pressed Cancel!";
						}
					}
					</script>
					<?php
					if (isset($_REQUEST['nuevo_programa'])) {
						$nuevo_programa = $_REQUEST['nuevo_programa'];
					} else {
						$nuevo_programa = "";
					}
                    if (isset($_REQUEST['ver_datos'])) {
						$ver_datos = $_REQUEST['ver_datos'];
					} else {
						$ver_datos = "";
					}
					if (isset($_REQUEST['modificar_programa'])) {
						$modificar_programa = $_REQUEST['modificar_programa'];
					} else {
						$modificar_programa = "";
					}
					if ($nuevo_programa=="1"){
						?>
						<a name="nuevo_programa">
						<form action="funciones.php" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<div class="row">
                                <div class="col-md-2 grid_box1">
                                    <input type="text" class="form-control1" name="numero" placeholder="Número de programa">
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control1" name="nombre" placeholder="Nombre del programa">
                                </div>
                                <div class="clearfix"> </div>
							</div>
						</div>
						</a>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<input type="text" class="form-control1" name="descripcion" placeholder="Descripción">
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<input type="text" class="form-control1" name="enlace" placeholder="Enlace">
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6 grid_box1">
                                    <input type="text" class="form-control1" name="nombre_imagen" placeholder="Nombre de la imagen">
                                </div>
                                <div class="col-md-6">
                                    <input type="file" name="imagen"/>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6 grid_box1">
                                    <input type="text" class="form-control1" name="fecha" placeholder="Fecha del programa">
                                </div>
                                <div class="col-md-6">
                                    <input type="hidden" name="nuevo_programa_radio" value="1"/>
									<input type="submit" name ="enviar" value="Subir_programa">
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        </form>
						<?php
					}
                    if ($ver_datos=="1"){
                        if (isset($_REQUEST['id_programa'])) {
                            $id_programa = $_REQUEST['id_programa'];
                        } else {
                            $id_programa = "";
                        }
                        $sql=" select * from radio where id=$id_programa";
                        $res = mysql_query($sql);
                        $res2 = mysql_fetch_array($res);
                        ?>
                        <a name="ver_datos">
						<div class="form-group">
							<div class="row">
                                <div class="col-md-2 grid_box1">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['numero'];?></span>
                                </div>
                                <div class="col-md-10">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['nombre'];?></span>
                                </div>
                                <div class="clearfix"> </div>
							</div>
						</div>
						</a>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['descripcion'];?></span>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-2 grid_box1">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['fecha'];?></span>
                                </div>
                                <div class="col-md-10">
                                    <span class="input-group-addon" id="basic-addon2"><a href="<?php echo $res2['enlace'];?>" target="_blank"><?php echo $res2['enlace'];?></a></span>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="input-group-addon" id="basic-addon2"><?php echo $res2['nombre_imagen'];?></span>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <img class="media-object" src="<?php echo $res2['direccion_imagen'];?>" alt="<?php echo $res2['nombre_imagen'];?>">
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <?php
                    }
					if (isset($_REQUEST['modificar_programa'])) {
						$modificar_programa = $_REQUEST['modificar_programa'];
					} else {
						$modificar_programa = "";
					}
					if ($modificar_programa=="1"){
						if (isset($_REQUEST['id_programa'])) {
                            $id_programa = $_REQUEST['id_programa'];
                        } else {
                            $id_programa = "";
                        }
                        $sql=" select * from radio where id=$id_programa";
                        $res = mysql_query($sql);
                        $res2 = mysql_fetch_array($res);
                        ?>
						<a name="nuevo_programa">
						<form action="funciones.php" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<div class="row">
                                <div class="col-md-2 grid_box1">
									<?php
									if ($res2['numero']<>""){
										?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Nro. Programa</span>
                                            <input type="text" class="form-control1" name="numero" value="<?php echo $res2['numero'];?>">
                                        </div>
                                        <?php
									} else {
										?>
										<input type="text" class="form-control1" name="numero" placeholder="Número de programa">
										<?php
									}		
									?>
								</div>
								<div class="col-md-10">
                                    <?php
                                    if ($res2['nombre']<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Nombre</span>
                                            <input type="text" class="form-control1" name="nombre" value="<?php echo $res2['nombre'];?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="nombre" placeholder="Nombre del programa">
                                       <?php
                                    }
                                    ?>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						</a>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
                                    <?php 
                                    if ($res2['descripcion']<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Descripción</span>
                                            <input type="text" class="form-control1" name="descripcion" value="<?php echo $res2['descripcion'];?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="descripcion" placeholder="Descripción">
                                        <?php    
                                    }
                                    ?>
                                </div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
                                    <?php
                                    if ($res2['enlace']<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Enlace</span>
                                            <input type="text" class="form-control1" name="enlace" value="<?php echo $res2['enlace'];?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="enlace" placeholder="Enlace">
                                        <?php    
                                    }
                                    ?>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    if ($res2['nombre_imagen']<>""){
                                        ?>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Nombre de la imagen</span>
                                            <input type="text" class="form-control1" name="nombre_imagen" value="<?php echo $res2['nombre_imagen'];?>">
                                        </div>    
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="nombre_imagen" placeholder="Nombre de la imagen">
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    if ($res2['direccion_imagen']<>""){
                                        ?>
                                        <img class="media-object" src="<?php echo $res2['direccion_imagen'];?>" alt="<?php echo $res2['nombre_imagen'];?>"><br>
                                        <input type="file" name="imagen"/>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="col-md-3 grid_box1">
                                            <input type="file" name="imagen"/>
                                        </div>
                                        <?php    
                                    }
                                    ?>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6 grid_box1">
                                    <?php
                                    if ($res2['fecha']<>""){
                                        ?>
                                        <span class="input-group-addon" id="basic-addon1">Fecha del programa</span>
                                        <input type="text" class="form-control1" name="fecha" value="<?php echo $res2['fecha'];?>">
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" class="form-control1" name="fecha" placeholder="Fecha del programa">
                                        <?php    
                                    }
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <input type="hidden" name="modificar_programa_radio" value="1"/>
                                    <input type="hidden" name="id_programa" value="<?php echo $res2['id'];?>"/>
									<input type="submit" name ="enviar" value="Subir_cambios">
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        </form>
						<?php
					}

                    if (($nuevo_programa=="") and ($modificar_programa=="") and ($ver_datos=="")){
                        ?>
                        <div class="in-right">
                            <form action="programa.php#nuevo_programa" method="post">
                                <input type="submit" value="Agregar nuevo programa de radio">
                                <input type="hidden" name="nuevo_programa" value="1">
                            </form>
                        </div>
                        <?php
                    }
                    ?>
					
				
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<?php
	require_once('pie.php');
	?>
</section>
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<script src="js/bootstrap.js"></script>
</body>
</html>
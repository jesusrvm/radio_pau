<?php
require_once('connection.php');
error_reporting(0);

if (isset($_REQUEST['nuevo_audio'])) {
    $nuevo_audio = $_REQUEST['nuevo_audio'];
} else {
    $nuevo_audio = "";
}
if (isset($_REQUEST['borrar_audio'])) {
    $borrar_audio = $_REQUEST['borrar_audio'];
} else {
    $borrar_audio = "";
}
if (isset($_REQUEST['predeterminar_audio'])) {
    $predeterminar_audio = $_REQUEST['predeterminar_audio'];
} else {
    $predeterminar_audio = "";
}
if (isset($_REQUEST['modificar_audio'])) {
    $modificar_audio = $_REQUEST['modificar_audio'];
} else {
    $modificar_audio = "";
}

if (isset($_REQUEST['nuevo_video'])) {
    $nuevo_video = $_REQUEST['nuevo_video'];
} else {
    $nuevo_video = "";
}
if (isset($_REQUEST['modificar_video'])) {
    $modificar_video = $_REQUEST['modificar_video'];
} else {
    $modificar_video = "";
}
if (isset($_REQUEST['borrar_video'])) {
    $borrar_video = $_REQUEST['borrar_video'];
} else {
    $borrar_video = "";
}

if (isset($_REQUEST['nuevo_post'])) {
    $nuevo_post = $_REQUEST['nuevo_post'];
} else {
    $nuevo_post = "";
}
if (isset($_REQUEST['modificar_post'])) {
    $modificar_post = $_REQUEST['modificar_post'];
} else {
    $modificar_post = "";
}
if (isset($_REQUEST['borrar_post'])) {
    $borrar_post = $_REQUEST['borrar_post'];
} else {
    $borrar_post = "";
}

if (isset($_REQUEST['nuevo_programa_radio'])) {
    $nuevo_programa_radio = $_REQUEST['nuevo_programa_radio'];
} else {
    $nuevo_programa_radio = "";
}
if (isset($_REQUEST['modificar_programa_radio'])) {
    $modificar_programa_radio = $_REQUEST['modificar_programa_radio'];
} else {
    $modificar_programa_radio = "";
}
if (isset($_REQUEST['borrar_programa_radio'])) {
    $borrar_programa_radio = $_REQUEST['borrar_programa_radio'];
} else {
    $borrar_programa_radio = "";
}

if (isset($_REQUEST['nuevo_suscriptor'])) {
    $nuevo_suscriptor = $_REQUEST['nuevo_suscriptor'];
} else {
    $nuevo_suscriptor = "";
}

if (isset($_REQUEST['nuevo_suscriptor_admin'])) {
    $nuevo_suscriptor_admin = $_REQUEST['nuevo_suscriptor_admin'];
} else {
    $nuevo_suscriptor_admin = "";
}
if (isset($_REQUEST['modificar_suscriptor'])) {
    $modificar_suscriptor = $_REQUEST['modificar_suscriptor'];
} else {
    $modificar_suscriptor = "";
}
if (isset($_REQUEST['borrar_suscriptor'])) {
    $borrar_suscriptor = $_REQUEST['borrar_suscriptor'];
} else {
    $borrar_suscriptor = "";
}

if (isset($_REQUEST['otras_opciones'])) {
    $otras_opciones = $_REQUEST['otras_opciones'];
} else {
    $otras_opciones = "";
}

function eliminar_simbolos($string){
 
    $string = trim($string);
 
    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
 
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
 
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
 
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
 
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
 
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );
 
    $string = str_replace(
        array("\\", "¨", "º", "-", "~",
             "#", "@", "|", "!", "\"",
             "·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "<code>", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":",
             " "),
        ' ',
        $string
    );
return $string;
}
///////////////////////////////////////////////////////////////////////////////////////
if ($nuevo_audio=="1"){
    if (isset($_REQUEST['nombre_imagen'])) {
        $nombre_imagen = $_REQUEST['nombre_imagen'];
    } else {
        $nombre_imagen = "";
    }
    if (isset($_REQUEST['nombre_audio'])) {
        $nombre_audio = $_REQUEST['nombre_audio'];
    } else {
        $nombre_audio = "";
    }
    
    $nombre_carpeta = $nombre_audio;
    $nombre_carpeta = eliminar_simbolos($nombre_carpeta);
    $prefijo = substr(md5(uniqid(rand())),0,6);
    $directorio =  "audios/".$prefijo.$nombre_carpeta;
    mkdir($directorio, 0777);
    $archivo_audio=$_FILES['audio']['name'];
    if ($archivo_audio<>""){
        $archivo_audio = eliminar_simbolos($archivo_audio);
        $prefijo = substr(md5(uniqid(rand())),0,6);
        $destino_audio =  $directorio."/".$prefijo.$archivo_audio;
        if (copy($_FILES['audio']['tmp_name'],$destino_audio)) {
            $exito2="22";
            $status = "El audio fue subido a <b>".$destino_audio."</b>";
        } else {
            $exito2="20";
            $status = "Error al subir el audio";
        }
    }
    $archivo_imagen=$_FILES['imagen']['name'];
    if ($archivo_imagen<>""){
        $archivo_imagen = eliminar_simbolos($archivo_imagen);
        $prefijo = substr(md5(uniqid(rand())),0,6);
        $destino_imagen =  $directorio."/".$prefijo.$archivo_imagen;
        if (copy($_FILES['imagen']['tmp_name'],$destino_imagen)) {
            $status = "La imagen fue subida a  <b>".$destino_imagen."</b>";
        } else {
            $status = "Error al subir la imagen";
        }
    }
    $db_table_name="audio";
    $insert_value = 'INSERT INTO `' . $database_mysql . '`.`'.$db_table_name.'`(`nombre_audio`,`direccion_audio`,`nombre_imagen`,`direccion_imagen`,`directorio`,`predeterminado`,`borrado`) VALUES ("' .$nombre_audio. '","' .$destino_audio. '","' .  $nombre_imagen . '","' . $destino_imagen . '","' . $directorio . '","0","0")';
    mysql_select_db($database_mysql, $dbh);
    $retry_value = mysql_query($insert_value, $dbh);
    
    $cont = 0;
    $sql=" select id from audio where (borrado=0)"; 
    $datos=mysql_query($sql,$dbh); 
    while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
        $cont++;
    }
    mysql_free_result($datos);
    
    $sSQL="Update otros_datos Set audios='$cont' Where id=1";
    mysql_query($sSQL);
        
    header("Location: admin.php");
}

if ($modificar_audio=="1"){
    if (isset($_REQUEST['id_audio'])) {
        $id_audio = $_REQUEST['id_audio'];
    } else {
        $id_audio = "";
    }
    if (isset($_REQUEST['nombre_imagen'])) {
        $nombre_imagen = $_REQUEST['nombre_imagen'];
    } else {
        $nombre_imagen = "";
    }
    if (isset($_REQUEST['nombre_audio'])) {
        $nombre_audio = $_REQUEST['nombre_audio'];
    } else {
        $nombre_audio = "";
    }
    $sql=" select direccion_imagen, direccion_audio, directorio from audio where id=$id_audio";
	$res = mysql_query($sql);
	$res2 = mysql_fetch_array($res);
	$direccion_imagen=$res2['direccion_imagen'];
    $direccion_audio=$res2['direccion_audio'];
    $directorio=$res2['directorio'];
    
    $archivo_audio=$_FILES['audio']['name'];
    $archivo_imagen=$_FILES['imagen']['name'];
    if (($archivo_audio<>"") and ($archivo_imagen<>"")){
        $archivo_audio = eliminar_simbolos($archivo_audio);
        $prefijo = substr(md5(uniqid(rand())),0,6);
        $destino_audio =  $directorio."/".$prefijo.$archivo_audio;
        if (copy($_FILES['audio']['tmp_name'],$destino_audio)) {
            $exito2="22";
            $status = "El audio fue subido a <b>".$destino_audio."</b>";
        } else {
            $exito2="20";
            $status = "Error al subir el audio";
        }
        $archivo_imagen = eliminar_simbolos($archivo_imagen);
        $prefijo = substr(md5(uniqid(rand())),0,6);
        $destino_imagen =  $directorio."/".$prefijo.$archivo_imagen;
        if (copy($_FILES['imagen']['tmp_name'],$destino_imagen)) {
            $status = "La imagen fue subida a  <b>".$destino_imagen."</b>";
        } else {
            $status = "Error al subir la imagen";
        }
        $sSQL="Update audio Set direccion_imagen='$destino_imagen' , direccion_audio='$destino_audio' Where id='$id_audio'";
        mysql_query($sSQL);
    } else if (($archivo_audio<>"") and ($archivo_imagen=="")){
        $archivo_audio = eliminar_simbolos($archivo_audio);
        $prefijo = substr(md5(uniqid(rand())),0,6);
        $destino_audio =  $directorio."/".$prefijo.$archivo_audio;
        if (copy($_FILES['audio']['tmp_name'],$destino_audio)) {
            $exito2="22";
            $status = "El audio fue subido a <b>".$destino_audio."</b>";
        } else {
            $exito2="20";
            $status = "Error al subir el audio";
        }
        $sSQL="Update audio Set direccion_audio='$destino_audio' Where id='$id_audio'";
        mysql_query($sSQL);
    } else if (($archivo_audio=="") and ($archivo_imagen<>"")){
        $archivo_imagen = eliminar_simbolos($archivo_imagen);
        $prefijo = substr(md5(uniqid(rand())),0,6);
        $destino_imagen =  $directorio."/".$prefijo.$archivo_imagen;
        if (copy($_FILES['imagen']['tmp_name'],$destino_imagen)) {
            $status = "La imagen fue subida a  <b>".$destino_imagen."</b>";
        } else {
            $status = "Error al subir la imagen";
        }
        $sSQL="Update audio Set direccion_imagen='$destino_imagen' Where id='$id_audio'";
        mysql_query($sSQL);
    }    
    
    $sSQL="Update audio Set nombre_audio='$nombre_audio' , nombre_imagen='$nombre_imagen' Where id='$id_audio'";
    mysql_query($sSQL);
    mysql_query($sSQL);
    header("Location: admin.php?modificar_audio=1&audio=1&id_audio=$id_audio#modificar_audio");
}    

if ($borrar_audio=="1"){
    if (isset($_REQUEST['id'])) {
        $id_audio = $_REQUEST['id'];
    } else {
        $id_audio = "";
    }
	$sSQL="Update audio Set borrado=1 Where id='$id_audio'";
    mysql_query($sSQL);
    
    $cont = 0;
    $sql=" select id from audio where (borrado=0)"; 
    $datos=mysql_query($sql,$dbh); 
    while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
        $cont++;
    }
    mysql_free_result($datos);
    
    $sSQL="Update otros_datos Set audios='$cont' Where id=1";
    mysql_query($sSQL);
    
	header("Location: admin.php?audio=1");
}
if ($predeterminar_audio=="1"){
	
	$sql=" select id from audio where predeterminado=1";
	$res = mysql_query($sql);
	$res2 = mysql_fetch_array($res);
	$id=$res2['id'];
	
	$sSQL="Update audio Set predeterminado=0 Where id='$id'";
    mysql_query($sSQL);
	
    if (isset($_REQUEST['id_audio'])) {
        $id_audio = $_REQUEST['id_audio'];
    } else {
        $id_audio = "";
    }
	$sSQL="Update audio Set predeterminado=1 Where id='$id_audio'";
    mysql_query($sSQL);
	header("Location: admin.php?audio=1");
}
//////////////////////////////////////////////////////////////////////////////////////
if ($nuevo_video=="1"){
    if (isset($_REQUEST['nombre'])) {
        $nombre = $_REQUEST['nombre'];
    } else {
        $nombre = "";
    }
    if (isset($_REQUEST['codigo'])) {
        $codigo = $_REQUEST['codigo'];
    } else {
        $codigo = "";
    }
    if (isset($_REQUEST['tipo'])) {
        $tipo = $_REQUEST['tipo'];
    } else {
        $tipo = "";
    }
    
    $db_table_name="video";
    $insert_value = 'INSERT INTO `' . $database_mysql . '`.`'.$db_table_name.'`(`nombre`,`codigo`,`tipo`,`borrado`) VALUES ("' .$nombre.'","' . $codigo . '","' . $tipo . '","0")';
    mysql_select_db($database_mysql, $dbh);
    $retry_value = mysql_query($insert_value, $dbh);
        
    $cont = 0;
    $sql=" select id from video where (borrado=0)"; 
    $datos=mysql_query($sql,$dbh); 
    while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
        $cont++;
    }
    mysql_free_result($datos);
    
    $sSQL="Update otros_datos Set videos='$cont' Where id=1";
    mysql_query($sSQL);
    
    header("Location: video.php?exito");
}
if ($modificar_video=="1"){
    if (isset($_REQUEST['id_video'])) {
        $id_video = $_REQUEST['id_video'];
    } else {
        $id_video = "";
    }
    if (isset($_REQUEST['nombre'])) {
        $nombre = $_REQUEST['nombre'];
    } else {
        $nombre = "";
    }
    if (isset($_REQUEST['codigo'])) {
        $codigo = $_REQUEST['codigo'];
    } else {
        $codigo = "";
    }
    if (isset($_REQUEST['tipo'])) {
        $tipo = $_REQUEST['tipo'];
    } else {
        $tipo = "";
    }
    $sSQL="Update video Set nombre='$nombre' , codigo='$codigo' , tipo='$tipo' Where id='$id_video'";
    mysql_query($sSQL);
    header("Location: video.php?id_video=$id_video");
}
if ($borrar_video=="1"){
    if (isset($_REQUEST['id_video'])) {
        $id_video = $_REQUEST['id_video'];
    } else {
        $id_video = "";
    }
    $sSQL="Update video Set borrado=1 Where id='$id_video'";
    mysql_query($sSQL);
    
    $cont = 0;
    $sql=" select id from video where (borrado=0)"; 
    $datos=mysql_query($sql,$dbh); 
    while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
        $cont++;
    }
    mysql_free_result($datos);
    
    $sSQL="Update otros_datos Set videos='$cont' Where id=1";
    mysql_query($sSQL);
    
    header("Location: video.php?id_video=$id_video");
}
/////////////////////////////////////////////////////////////////////////////////////////
if ($nuevo_post=="1"){
    if (isset($_REQUEST['numero'])) {
        $numero = $_REQUEST['numero'];
    } else {
        $numero = "";
    }
    if (isset($_REQUEST['nombre'])) {
        $nombre = $_REQUEST['nombre'];
    } else {
        $nombre = "";
    }
    if (isset($_REQUEST['parrafo_1'])) {
        $parrafo_1 = $_REQUEST['parrafo_1'];
    } else {
        $parrafo_1 = "";
    }
    if (isset($_REQUEST['parrafo_2'])) {
        $parrafo_2 = $_REQUEST['parrafo_2'];
    } else {
        $parrafo_2 = "";
    }
    if (isset($_REQUEST['parrafo_3'])) {
        $parrafo_3 = $_REQUEST['parrafo_3'];
    } else {
        $parrafo_3 = "";
    }
    if (isset($_REQUEST['codigo'])) {
        $codigo = $_REQUEST['codigo'];
    } else {
        $codigo = "";
    }
    if (isset($_REQUEST['nombre_imagen'])) {
        $nombre_imagen = $_REQUEST['nombre_imagen'];
    } else {
        $nombre_imagen = "";
    }
        
    $dia=date("d");
    $mes=date("m");
    $anio=date("Y");
    
    $nombre_carpeta = $nombre;
    $nombre_carpeta = eliminar_simbolos($nombre_carpeta);
    $prefijo = substr(md5(uniqid(rand())),0,6);
    $directorio =  "post/".$prefijo.$nombre_carpeta;
    mkdir($directorio, 0777);
    
    $archivo_imagen=$_FILES['imagen']['name'];
    if ($archivo_imagen<>""){
        $archivo_imagen = eliminar_simbolos($archivo_imagen);
		
        $prefijo = substr(md5(uniqid(rand())),0,6);
        $direccion_imagen =  $directorio."/".$prefijo.$archivo_imagen;
        if (copy($_FILES['imagen']['tmp_name'],$direccion_imagen)) {
            $status = "La imagen fue subida a  <b>".$direccion_imagen."</b>";
        } else {
            $status = "Error al subir la imagen";
        }
    }
    $db_table_name="post";
    
    $insert_value = 'INSERT INTO `' . $database_mysql . '`.`'.$db_table_name.'`(`nombre`,`parrafo_1`,`parrafo_2`,`parrafo_3`,`imagen`,`direccion_imagen`,`codigo`,`numero`,`anio`,`mes`,`dia`,`directorio`,`borrado`) VALUES ("' .$nombre.'","' . $parrafo_1 . '","' . $parrafo_2 . '","' . $parrafo_3 . '","' . $nombre_imagen . '","' . $direccion_imagen . '","' . $codigo . '","' . $numero . '","' .$anio. '","' .$mes. '","' . $dia . '","' . $directorio . '","0")';
    
    mysql_select_db($database_mysql, $dbh);
    $retry_value = mysql_query($insert_value, $dbh);
    
    $cont = 0;
    $sql=" select id from post where (borrado=0)"; 
    $datos=mysql_query($sql,$dbh); 
    while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
        $cont++;
    }
    mysql_free_result($datos);
    
    $sSQL="Update otros_datos Set post='$cont' Where id=1";
    mysql_query($sSQL);
        
    header("Location: post.php");
}

if ($modificar_post=="1"){
    if (isset($_REQUEST['id_post'])) {
        $id_post = $_REQUEST['id_post'];
    } else {
        $id_post = "";
    }
    if (isset($_REQUEST['numero'])) {
        $numero = $_REQUEST['numero'];
    } else {
        $numero = "";
    }
    if (isset($_REQUEST['nombre'])) {
        $nombre = $_REQUEST['nombre'];
    } else {
        $nombre = "";
    }
    if (isset($_REQUEST['parrafo_1'])) {
        $parrafo_1 = $_REQUEST['parrafo_1'];
    } else {
        $parrafo_1 = "";
    }
    if (isset($_REQUEST['parrafo_2'])) {
        $parrafo_2 = $_REQUEST['parrafo_2'];
    } else {
        $parrafo_2 = "";
    }
    if (isset($_REQUEST['parrafo_3'])) {
        $parrafo_3 = $_REQUEST['parrafo_3'];
    } else {
        $parrafo_3 = "";
    }
    if (isset($_REQUEST['codigo'])) {
        $codigo = $_REQUEST['codigo'];
    } else {
        $codigo = "";
    }
    if (isset($_REQUEST['nombre_imagen'])) {
        $nombre_imagen = $_REQUEST['nombre_imagen'];
    } else {
        $nombre_imagen = "";
    }
    
    $dia = date("d");
    $mes = date("m");
    $anio = date("Y");
        
    $sql=" select direccion_imagen, directorio from post where id='$id_post'";
	$res = mysql_query($sql);
	$res2 = mysql_fetch_array($res);
	$direccion_imagen=$res2['direccion_imagen'];
    $directorio=$res2['directorio'];
    
    $archivo_imagen=$_FILES['imagen']['name'];
    if($archivo_imagen<>""){
		$archivo_imagen = eliminar_simbolos($archivo_imagen);
		$prefijo = substr(md5(uniqid(rand())),0,6);
        $destino_imagen =  $directorio."/".$prefijo.$archivo_imagen;
		if (copy($_FILES['imagen']['tmp_name'],$destino_imagen)) {
            $status = "La imagen fue subida a  <b>".$destino_imagen."</b>";
        } else {
            $status = "Error al subir la imagen";
        }
		$sSQL="Update post Set direccion_imagen='$destino_imagen' Where id='$id_post'";
        mysql_query($sSQL);
    }    
    
    $sSQL="Update post Set nombre='$nombre' , parrafo_1='$parrafo_1' , parrafo_2='$parrafo_2' , parrafo_3='$parrafo_3', imagen='$nombre_imagen' , codigo='$codigo' , numero='$numero', anio='$anio', mes='$mes', dia='$dia' Where id='$id_post'";
    mysql_query($sSQL);
    
    header("Location: post.php?ver_datos=1&id_post=$id_post");
}    

if ($borrar_post=="1"){
    if (isset($_REQUEST['id'])) {
        $id_post = $_REQUEST['id'];
    } else {
        $id_post = "";
    }
	$sSQL="Update post Set borrado=1 Where id='$id_post'";
    mysql_query($sSQL);
    
    $cont = 0;
    $sql=" select id from post where (borrado=0)"; 
    $datos=mysql_query($sql,$dbh); 
    while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
        $cont++;
    }
    mysql_free_result($datos);
    
    $sSQL="Update otros_datos Set post='$cont' Where id=1";
    mysql_query($sSQL);
    
	header("Location: post.php");
}
/////////////////////////////////////////////////////////////////////////////////////
if ($nuevo_programa_radio=="1"){
    if (isset($_REQUEST['numero'])) {
        $numero = $_REQUEST['numero'];
    } else {
        $numero = "";
    }
    if (isset($_REQUEST['nombre'])) {
        $nombre = $_REQUEST['nombre'];
    } else {
        $nombre = "";
    }
    if (isset($_REQUEST['descripcion'])) {
        $descripcion = $_REQUEST['descripcion'];
    } else {
        $descripcion = "";
    }
    if (isset($_REQUEST['enlace'])) {
        $enlace = $_REQUEST['enlace'];
    } else {
        $enlace = "";
    }
    if (isset($_REQUEST['nombre_imagen'])) {
        $nombre_imagen = $_REQUEST['nombre_imagen'];
    } else {
        $nombre_imagen = "";
    }
    if (isset($_REQUEST['fecha'])) {
        $fecha = $_REQUEST['fecha'];
    } else {
        $fecha = "";
    }
        
    $dia=date("d");
    $mes=date("m");
    $anio=date("Y");
    
    $nombre_carpeta = $nombre;
    $nombre_carpeta = eliminar_simbolos($nombre_carpeta);
    $prefijo = substr(md5(uniqid(rand())),0,6);
    $directorio =  "programas_radio/".$prefijo.$nombre_carpeta;
    mkdir($directorio, 0777);
    
    $archivo_imagen=$_FILES['imagen']['name'];
    if ($archivo_imagen<>""){
        $archivo_imagen = eliminar_simbolos($archivo_imagen);
		
        $prefijo = substr(md5(uniqid(rand())),0,6);
        $direccion_imagen =  $directorio."/".$prefijo.$archivo_imagen;
        if (copy($_FILES['imagen']['tmp_name'],$direccion_imagen)) {
            $status = "La imagen fue subida a  <b>".$direccion_imagen."</b>";
        } else {
            $status = "Error al subir la imagen";
        }
    }
    $db_table_name="radio";
    
    $insert_value = 'INSERT INTO `' . $database_mysql . '`.`'.$db_table_name.'`(`nombre`,`numero`,`enlace`,`descripcion`,`fecha`,`nombre_imagen`,`direccion_imagen`,`borrado`) VALUES ("' .$nombre.'","' . $numero . '","' . $enlace . '","' . $descripcion . '","' . $fecha . '","' . $nombre_imagen . '","' . $direccion_imagen . '","0")';
    mysql_select_db($database_mysql, $dbh);
    $retry_value = mysql_query($insert_value, $dbh);
    
    $cont = 0;
    $sql=" select id from radio where (borrado=0)"; 
    $datos=mysql_query($sql,$dbh); 
    while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
        $cont++;
    }
    mysql_free_result($datos);
    
    $sSQL="Update otros_datos Set programas_radio='$cont' Where id=1";
    mysql_query($sSQL);
        
    header("Location: programa.php");
}

if ($modificar_programa_radio=="1"){
    if (isset($_REQUEST['id_programa'])) {
        $id_programa = $_REQUEST['id_programa'];
    } else {
        $id_programa = "";
    }
    if (isset($_REQUEST['numero'])) {
        $numero = $_REQUEST['numero'];
    } else {
        $numero = "";
    }
    if (isset($_REQUEST['nombre'])) {
        $nombre = $_REQUEST['nombre'];
    } else {
        $nombre = "";
    }
    if (isset($_REQUEST['descripcion'])) {
        $descripcion = $_REQUEST['descripcion'];
    } else {
        $descripcion = "";
    }
    if (isset($_REQUEST['fecha'])) {
        $fecha = $_REQUEST['fecha'];
    } else {
        $fecha = "";
    }
    if (isset($_REQUEST['enlace'])) {
        $enlace = $_REQUEST['enlace'];
    } else {
        $enlace = "";
    }
    if (isset($_REQUEST['nombre_imagen'])) {
        $nombre_imagen = $_REQUEST['nombre_imagen'];
    } else {
        $nombre_imagen = "";
    }
    
    $dia = date("d");
    $mes = date("m");
    $anio = date("Y");
        
    $sql=" select direccion_imagen, directorio from radio where id='$id_programa'";
	$res = mysql_query($sql);
	$res2 = mysql_fetch_array($res);
	$direccion_imagen=$res2['direccion_imagen'];
    $directorio=$res2['directorio'];
    
    $archivo_imagen=$_FILES['imagen']['name'];
    if($archivo_imagen<>""){
		$archivo_imagen = eliminar_simbolos($archivo_imagen);
		$prefijo = substr(md5(uniqid(rand())),0,6);
        $destino_imagen =  $directorio."/".$prefijo.$archivo_imagen;
		if (copy($_FILES['imagen']['tmp_name'],$destino_imagen)) {
            $status = "La imagen fue subida a  <b>".$destino_imagen."</b>";
        } else {
            $status = "Error al subir la imagen";
        }
		$sSQL="Update radio Set direccion_imagen='$destino_imagen' Where id='$id_programa'";
        mysql_query($sSQL);
    }    
    
    $sSQL="Update radio Set nombre='$nombre' , descripcion='$descripcion' , nombre_imagen='$nombre_imagen' , enlace='$enlace' , numero='$numero', fecha='$fecha' Where id='$id_programa'";
    mysql_query($sSQL);
    
    header("Location: programa.php?ver_datos=1&id_programa=$id_programa");
}    

if ($borrar_programa_radio=="1"){
    if (isset($_REQUEST['id'])) {
        $id_programa = $_REQUEST['id'];
    } else {
        $id_programa = "";
    }
	$sSQL="Update radio Set borrado=1 Where id='$id_programa'";
    mysql_query($sSQL);
    
    $cont = 0;
    $sql=" select id from radio where (borrado=0)"; 
    $datos=mysql_query($sql,$dbh); 
    while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
        $cont++;
    }
    mysql_free_result($datos);
    
    $sSQL="Update otros_datos Set programas_radio='$cont' Where id=1";
    mysql_query($sSQL);
    
	header("Location: programa.php");
}

///////////////////////////////SUCRIPTOR////////////////////////////////////////////

if ($nuevo_suscriptor=="1"){
    $email = trim($_POST['email']);
    $Ok = ereg("^([a-zA-Z0-9_\.-]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", $email);
    if ($Ok) {
        //mail($emailmanager,'Subscribe','','From: '.$email);

        //if(!ereg("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$",$UNameFrm))
        //{
            
            if (isset($_REQUEST['email'])){
                $email = $_REQUEST['email'];
            } else {
                $email = "";
            }
            
            $db_table_name="correo";
            $insert_value = 'INSERT INTO `' . $database_mysql . '`.`'.$db_table_name.'` (`nombre`,`correo`,`borrado`) VALUES ("","' .$email. '","0")';
            mysql_select_db($database_mysql, $dbh);
            $retry_value = mysql_query($insert_value, $dbh);
            
            $cont = 0;
            $sql=" select id from correo where (borrado=0)"; 
            $datos=mysql_query($sql,$dbh); 
            while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
                $cont++;
            }
            mysql_free_result($datos);
            
            $sSQL="Update otros_datos Set suscriptores='$cont' Where id=1";
            mysql_query($sSQL);
            
            header("Location: index.php?exito_suscriptor=1#suscriptor");
        //}
    } else {
        if(!ereg("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$",$UNameFrm))
        {
            header("Location: index.php?exito_suscriptor=0#suscriptor");
        }
    }
}

if ($nuevo_suscriptor_admin=="1"){
    if (isset($_REQUEST['nombre'])) {
        $nombre = $_REQUEST['nombre'];
    } else {
        $nombre = "";
    }
    if (isset($_REQUEST['correo'])) {
        $correo = $_REQUEST['correo'];
    } else {
        $correo = "";
    }
    
    $db_table_name="correo";
    $insert_value = 'INSERT INTO `' . $database_mysql . '`.`'.$db_table_name.'` (`nombre`,`correo`,`borrado`) VALUES ("' .$nombre. '","' .$correo. '","0")';
    mysql_select_db($database_mysql, $dbh);
    $retry_value = mysql_query($insert_value, $dbh);
    
    $cont = 0;
    $sql=" select id from correo where (borrado=0)"; 
    $datos=mysql_query($sql,$dbh); 
    while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
        $cont++;
    }
    mysql_free_result($datos);
    
    $sSQL="Update otros_datos Set suscriptores='$cont' Where id=1";
    mysql_query($sSQL);
    
    header("Location: suscriptores.php");
}

if ($modificar_suscriptor=="1"){
    if (isset($_REQUEST['id_suscriptor'])) {
        $id_suscriptor = $_REQUEST['id_suscriptor'];
    } else {
        $id_suscriptor = "";
    }
    if (isset($_REQUEST['nombre'])) {
        $nombre = $_REQUEST['nombre'];
    } else {
        $nombre = "";
    }
    if (isset($_REQUEST['correo'])) {
        $correo = $_REQUEST['correo'];
    } else {
        $correo = "";
    }
    $sSQL="Update correo Set nombre='$nombre', correo='$correo' Where id='$id_suscriptor'";
    mysql_query($sSQL);
    
    header("Location: suscriptores.php?ver_datos=1&id_suscriptor=$id_suscriptor");
}

if ($borrar_suscriptor=="1"){
    if (isset($_REQUEST['id'])) {
        $id_suscriptor = $_REQUEST['id'];
    } else {
        $id_suscriptor = "";
    }
    $sSQL="Update correo Set borrado=1 Where id='$id_suscriptor'";
    mysql_query($sSQL);
    
    $cont = 0;
    $sql=" select id from correo where (borrado=0)"; 
    $datos=mysql_query($sql,$dbh); 
    while (($row=mysql_fetch_array($datos)) and ($ban==0)) {
        $cont++;
    }
    mysql_free_result($datos);
    
    $sSQL="Update otros_datos Set suscriptores='$cont' Where id=1";
    mysql_query($sSQL);
    
    header("Location: suscriptores.php");
}

if (isset($_REQUEST['descargar_correo'])) {
	$descargar_correo = $_REQUEST['descargar_correo'];
} else {
	$descargar_correo = "";
}
if ($descargar_correo=="1"){
	
	// output headers so that the file is downloaded rather than displayed
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=data.csv');

	// create a file pointer connected to the output stream
	$output = fopen('php://output', 'w');

	// output the column headings Se agregan en array los nombre de las columnas a exportar
	//fputcsv($output, array('Column 1','Column 2','Column 3'));
	fputcsv($output);//si borro array no se agrega la linea al principio con los nombres de las columnas

	// fetch the data
	// mysql_connect('localhost', 'username', 'password');
	// mysql_select_db('database');
	$rows = mysql_query('SELECT correo FROM correo where borrado=0');

	// loop over the rows, outputting them
	while ($row = mysql_fetch_assoc($rows)) fputcsv($output, $row);
}

if ($otras_opciones=="1"){
    if (isset($_REQUEST['direccion'])) {
        $direccion = $_REQUEST['direccion'];
    } else {
        $direccion = "";
    }
    if (isset($_REQUEST['color'])) {
        $color = $_REQUEST['color'];
    } else {
        $color = "";
    }
    if (isset($_REQUEST['texto'])) {
        $texto = $_REQUEST['texto'];
    } else {
        $texto = "";
    }
    if ($direccion == "1"){
        if (isset($_REQUEST['direccion_pagina'])) {
            $direccion_pagina = $_REQUEST['direccion_pagina'];
        } else {
            $direccion_pagina = "";
        }
        $sSQL="Update otros_datos Set direccion_pagina='$direccion_pagina' Where id=1";
        mysql_query($sSQL);
        header("Location: otras_opciones.php?direccion=1");
        
    } elseif ($color == "1"){
        
        if (isset($_REQUEST['favcolor_1'])) {
            $favcolor_1 = $_REQUEST['favcolor_1'];
        } else {
            $favcolor_1 = "";
        }
        $sSQL="Update otros_datos Set color_1='$favcolor_1' Where id=1";
        mysql_query($sSQL);
        
        if (isset($_REQUEST['favcolor_2'])) {
            $favcolor_2 = $_REQUEST['favcolor_2'];
        } else {
            $favcolor_2 = "";
        }
        $sSQL="Update otros_datos Set color_2='$favcolor_2' Where id=1";
        mysql_query($sSQL);
        header("Location: otras_opciones.php?color=1");
    } elseif ($texto == "1"){
        if (isset($_REQUEST['texto_1'])) {
            $texto_1 = $_REQUEST['texto_1'];
        } else {
            $texto_1 = "";
        }
        if (isset($_REQUEST['texto_2'])) {
            $texto_2 = $_REQUEST['texto_2'];
        } else {
            $texto_2 = "";
        }
        if (isset($_REQUEST['texto_3'])) {
            $texto_3 = $_REQUEST['texto_3'];
        } else {
            $texto_3 = "";
        }
        if (isset($_REQUEST['texto_4'])) {
            $texto_4 = $_REQUEST['texto_4'];
        } else {
            $texto_4 = "";
        }
        if (isset($_REQUEST['texto_5'])) {
            $texto_5 = $_REQUEST['texto_5'];
        } else {
            $texto_5 = "";
        }
        if (isset($_REQUEST['texto_6'])) {
            $texto_6 = $_REQUEST['texto_6'];
        } else {
            $texto_6 = "";
        }
        if (isset($_REQUEST['texto_7'])) {
            $texto_7 = $_REQUEST['texto_7'];
        } else {
            $texto_7 = "";
        }
        if (isset($_REQUEST['texto_8'])) {
            $texto_8 = $_REQUEST['texto_8'];
        } else {
            $texto_8 = "";
        }
        if (isset($_REQUEST['texto_9'])) {
            $texto_9 = $_REQUEST['texto_9'];
        } else {
            $texto_9 = "";
        }
        if (isset($_REQUEST['texto_10'])) {
            $texto_10 = $_REQUEST['texto_10'];
        } else {
            $texto_10 = "";
        }
        if (isset($_REQUEST['texto_11'])) {
            $texto_11 = $_REQUEST['texto_11'];
        } else {
            $texto_11 = "";
        }
        $sSQL="Update otros_datos Set texto_1='$texto_1', texto_2='$texto_2', texto_3='$texto_3', texto_4='$texto_4', texto_5='$texto_5', texto_6='$texto_6', texto_7='$texto_7', texto_8='$texto_8', texto_9='$texto_9', texto_10='$texto_10', texto_11='$texto_11' Where id=1";
        mysql_query($sSQL);
        header("Location: otras_opciones.php?texto=1&texto5=$texto5");
    }
}
?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Programas | Con M de memoria</title>
<?php
require_once('meta.php');
?>
<!-- You can use open graph tags to customize link previews.-->
		
		<meta property="og:url"           content="http://localhost/radio/radio.php" />
		<meta property="og:type"          content="website" />
		<meta property="og:title"         content="Con M de memoria" />
		<meta property="og:image"         content="images/con-m-de-memoria.jpg"/>
		<meta property="og:description" content="Emisiones de Con M de memoria" />
</head> 
    	 <!-- /w3layouts-agile -->
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
      <!-- left side start-->
		<?php
        require_once('connection.php');
        require_once('menu.php');
        ?>
		 	 <!-- /w3l-agile -->
		<!-- left side end-->
					<!-- app-->
			<?php
            require_once('app.php');
            ?>
			<!-- //app-->
			 	 <!-- /w3l-agile -->
		<!-- signup -->
			<?php
            require_once('registro.php');
            ?>
			<!-- //signup -->
 	 <!-- /w3layouts-agile -->
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php
            require_once('cabecera.php');
            ?>
			<!-- //header-ends -->
 	 <!-- /w3l-agile -->
		<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="inner-content">
				      <div class="music-browse">
					<!--albums-->
					<?php
                    $sql="select texto_6 from otros_datos where id=1";
                    $res = mysql_query($sql);
                    $res2 = mysql_fetch_array($res);
                    $texto_6=$res2['texto_6'];
                    ?>
					
						<div class="browse">
                            <div class="tittle-head two">
                                <h3 class="tittle"><?php echo $texto_6;?><span class="new">Escuchar</span></h3>
                                <!--<a href="browse.php"><h4 class="tittle third">See all</h4></a>-->
                                <div class="clearfix"> </div>
                            </div>
							
                            <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
								
                                <!--<ul id="myTab" class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Telugu</a>
                                    </li>
								    <li role="presentation" class="">
                                        <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Hindi</a>
                                    </li>
								    <li role="presentation" class="dropdown">
                                        <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">English <span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                                            <li>
                                                <a href="#dropdown1" tabindex="-1" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1">Melody</a>
                                            </li>
                                            <li>
                                                <a href="#dropdown2" tabindex="-1" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2">Classic</a>
                                            </li>
										</ul>
								    </li>
								    <li role="presentation">
                                        <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Tamil</a>
                                    </li>
								    <li role="presentation" class="">
                                        <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Malayalam</a>
                                    </li>
								</ul>-->
								
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
                                        <div class="browse-inner">
                                        <!-- /agileits -->
                                            <?php
                                            $sql=" select * from radio where (borrado=0) order by id desc"; 
                                            $datos=mysql_query($sql,$dbh); 
                                            while ($row=mysql_fetch_array($datos)) {
                                                $id_nuevo_audio=$row['nombre']." ".$row['numero'];
                                                $id_nuevo_audio = str_replace(" ", "_",$id_nuevo_audio);//Reemplaza espacios en blanco por _
                                                ?>
                                                <div class="col-md-3 artist-grid">
                                                <a  href="nuevo_audio.php?programa=<?php echo $id_nuevo_audio; ?>"><img src="<?php echo $row['direccion_imagen'];?>" title="<?php echo $row['nombre_imagen'];?>"></a>
                                                <a href="nuevo_audio.php?programa=<?php echo $id_nuevo_audio; ?>"><i class="glyphicon glyphicon-play-circle"></i></a>
                                                <a class="art" href="nuevo_audio.php?programa=<?php echo $id_nuevo_audio; ?>"><?php echo $row['numero'].". ".$row['nombre'];?></a>
                                                </div>
                                                <?php                                       
                                            }
                                            mysql_free_result($datos);
                                            ?>
                                            <div class="clearfix"> </div>    
										</div>
                                    </div>
                                    
								</div>
							</div>
						 	 <!-- /agileinfo -->
						</div>
					<!--//End-albums-->
					
						
					</div>
								<!--//discover-view-->
							<!--//music-left-->
				</div>
						
			</div>
			<div class="clearfix"></div>
						<!--body wrapper end-->
						<!-- /w3layouts-agile -->
		</div>
			  <!--body wrapper end-->
			     <?php
                 require_once('pie.php');
                 ?>
        </div>
        <!--footer section start-->
			<!--<footer>
			   <p>&copy 2016 Mosaic. All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">w3layouts.</a></p>
			</footer>-->
        <!--footer section end-->
 	    <!-- /w3layouts-agile -->
        <!-- main content end-->
   </section>
   	 <!-- /wthree-agile -->
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.js"></script>
</body>
</html>
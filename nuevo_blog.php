<?php
if (isset($_REQUEST['id'])) {
    $id_nombre_post = $_REQUEST['id'];
} else {
    $id_nombre_post = "";
}
$id_nombre_post = str_replace("_", " ",$id_nombre_post);//Reemplaza _ por espacios en blanco
$nombre_post = substr($id_nombre_post, 0, -7);  // devuelve el nombre del evento menos los últimos 7 carácteres que son la fecha

$nombre_post = trim($nombre_post);//elimina espacios en blanco al inicio y final de la cadena

$fecha_post = substr($id_nombre_post, -7); // devuelve los últimos 7 carácteres que es la fecha

$anio_post=substr($fecha_post, -4);
$mes_post=substr($fecha_post, 0, 2);

require_once('connection.php');
$sql="select * from post where ((nombre='$nombre_post') and (mes='$mes_post') and (anio='$anio_post'))";
$res = mysql_query($sql);
$res2 = mysql_fetch_array($res);
$id_post=$res2['id'];
$nombre=$res2['nombre'];
$parrafo_1=$res2['parrafo_1'];
$parrafo_2=$res2['parrafo_2'];
$parrafo_3=$res2['parrafo_3'];
$dia=$res2['dia'];
$mes=$res2['mes'];
$anio=$res2['anio'];
$imagen=$res2['imagen'];
$direccion_imagen=$res2['direccion_imagen'];
$codigo=$res2['codigo'];
function dameURL(){
    $url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
    return $url;
}

$nombre_imagen = str_replace(" ", "%20", $imagen);

?>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $nombre. ' | Con "M" de memoria'; ?></title>
<?php
require_once('meta.php');
$direccion_imagen = $direccion_pagina.$nombre_imagen;
?>
<!-- You can use open graph tags to customize link previews.-->
		
    <meta property="og:title" content="<?php echo "Audios musicales. ".$nombre;?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo dameURL();?>" />
    <meta property="og:image" content="<?php echo $direccion_imagen; ?>"/>
    <meta property="og:image:secure_url" content="<?php echo $direccion_imagen;?>" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="400" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="<?php echo $imagen;?>" />
    <meta property="og:description" content="<?php echo $parrafo_1." - ".$dia."/".$mes."/".$anio;?>" />
</head> 
   <!-- /w3layouts -->
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
      
    
    <section>
      <!-- left side start-->
		<?php
        require_once('menu.php');
        ?>
		<!-- left side end-->
		 <!-- /agileinfo -->
					<!-- app-->
			<?php
            require_once('app.php');
            ?>
			<!-- //app-->
			 <!-- /agile-its -->
		<!-- signup -->
			<?php
            require_once('registro.php');
            ?>
			<!-- //signup -->
	 <!-- /w3layouts-agile -->
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php
            require_once('cabecera.php');
            ?>
			<!--<pre><?php //echo "Nombre post: ".$nombre_post." - anio post: ".$anio_post." - mes post: ".$mes_post." id post: ".$id_post;?></pre>-->
					 <!-- /w3l-agile -->
				<!--notification menu end -->
				<!-- //header-ends -->
					<div id="page-wrapper">
                        <div class="inner-content">
                            <!-- /blog -->
                            
                            <!--<div class="tittle-head">
                                <h3 class="tittle">Blogs</h3>
                                <div class="clearfix"> </div>
                            </div>-->
                            <!-- /music-left -->
                            <div class="music-left">
                                    <div class="post-media">
                                        <a href=""><img src="<?php echo $direccion_imagen;?>" class="img-responsive" alt="<?php echo $imagen;?>" /></a>
                                        <div class="blog-text">
                                            <a href=""><h3 class="h-t"><?php echo $nombre;?></h3></a>
                                            <div class="entry-meta">
                                                <h6 class="blg"><i class="fa fa-clock-o"></i><?php echo $dia."-".$mes_post."-".$anio_post;?> <!--Jan 25, 2016--></h6>
                                                <div class="icons">
                                                       
                                                
                                                    <!-- Facebook -->
                                                    <a href="http://www.facebook.com/sharer.php?u=<?php echo dameURL();?>" target="_blank">
                                                        <img src="images/facebook.png" alt="Facebook" class="share-buttons"/>
                                                    </a>
                                                    <!-- Twitter -->
                                                    <a href="https://twitter.com/share?url=<?php echo dameURL();?>" target="_blank">
                                                        <img src="images/twitter.png" alt="Twitter" class="share-buttons"/>
                                                    </a>
                                                    <!-- Whatsapp -->
                                                    <a href="whatsapp://send?text=<?php echo dameURL();?>" target="_blank"><img src="images/whatsapp.png" width="32"></a>
                                                    
                                                    <!-- Linkendin -->
                                                    
                                                    <!--<a href="https://www.linkedin.com/cws/share?url=<?php //echo dameURL();?>" target="_blank"><img src="images/linkedin.png" width="32"></a>-->
                                                    
                                                    <!-- Google+ -->
                                                    <!--<a href="https://plus.google.com/share?url=<?php //echo dameURL();?>" target="_blank">
                                                        <img src="images/google.png" alt="Google" class="share-buttons"/>
                                                    </a>-->
                                                    <!-- Email -->
                                                    <a href="mailto:?Subject=Reunión UMA 2016&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20<?php echo dameURL();?>">
                                                        <img src="images/email.png" alt="Email" class="share-buttons"/>
                                                    </a>
                                                    <!-- Print -->
                                                    <a href="javascript:;" onclick="window.print()">
                                                        <img src="images/print.png" alt="Print" class="share-buttons"/>
                                                    </a>
                                                             
                                                
                                                    <!--<a href="#"><i class="fa fa-user"></i> Admin</a>
                                                    <a href="#"><i class="fa fa-comments-o"></i> 2</a>
                                                    <a href="#"><i class="fa fa-thumbs-o-up"></i> 152</a>
                                                    <a href="#"><i class="fa fa-thumbs-o-down"></i>  26</a>-->
                                                </div>
                                                <div class="clearfix"></div>
                                                <p><?php echo $parrafo_1;?></p>
                                                <p><?php echo $parrafo_2;?></p>
                                                <p><?php echo $parrafo_3;?></p>
                                                <p><?php echo $codigo;?></p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div id="disqus_thread"></div>
                                    <script>

                                    /**
                                    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                                    /*
                                    var disqus_config = function () {
                                    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                                    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                    };
                                    */
                                    (function() { // DON'T EDIT BELOW THIS LINE
                                    var d = document, s = d.createElement('script');
                                    s.src = 'https://http-wxuwlxzn-lucusvirtual-es.disqus.com/embed.js';
                                    s.setAttribute('data-timestamp', +new Date());
                                    (d.head || d.body).appendChild(s);
                                    })();
                                    </script>
                                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                    
                            </div>
                            <!-- //music-left-->
                            <!-- /music-right-->
                                
                            <?php
                            require_once('lateral_derecho.php');
                            ?>    
                            
                            <div class="clearfix"></div>
                            <!-- //blog -->
                        </div>
                                                
                        <div class="clearfix"></div>
						<!--body wrapper end-->
            <!-- /w3l-agile -->
					</div>
			  <!--body wrapper end-->
			     <?php
                 require_once('pie.php');
                 ?>
		</div>
		<!-- /wthree-agile -->
        <!--footer section start-->
			<!--<footer>
			   <p>&copy 2016 Mosaic. All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">w3layouts.</a></p>
			</footer>-->
        <!--footer section end-->

      <!-- main content end-->
   </section>
  
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.js"></script>
</body>
</html>
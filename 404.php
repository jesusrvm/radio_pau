<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Mosaic a Entertainment Category Flat Bootstrap Responsive Website Template | 404 :: w3layouts</title>
<?php
require_once('meta.php');
?>
</head> 
   	 <!-- /w3layouts-agile -->
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
      <!-- left side start-->
		<?php
        require_once('menu.php');
        ?>
		<!-- left side end-->
			 <!-- /w3layouts-agile -->
					<!-- app-->
			<?php
            require_once('app.php');
            ?>
			<!-- //app-->
				 <!-- /w3l-agile -->
		<!-- signup -->
			<?php
            require_once('registro.php');
            ?>
			<!-- //signup -->
	 <!-- /w3l-agile -->
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php
            require_once('cabecera.php');
            ?>
			<!-- //header-ends -->
	 <!-- /agileits -->
					<!-- //header-ends -->
						<div id="page-wrapper">
						<div class="inner-content">
						
									<!-- /error_page -->
									<div class="error-top">
										<img src="images/pic_error.png" alt="" />
										<h3>Page Not Found...<h3>
										<div class="clearfix"></div>
										
										<div class="error">
											<a class="not" href="index.php">Back To Home</a>
										</div>

									<!-- //error_page -->
								</div>
								
							<div class="clearfix"></div>
						<!--body wrapper end-->
						</div>		
					</div>
						 <!-- /agileinfo -->
			  <!--body wrapper end-->
			     <?php
                 require_once('pie.php');
                 ?>
			</div>
				 <!-- /wthree-agile -->
        <!--footer section start-->
			<!--<footer>
			   <p>&copy 2016 Mosaic. All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">w3layouts.</a></p>
			</footer>-->
        <!--footer section end-->

      <!-- main content end-->
   </section>
  
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.js"></script>
</body>
</html>
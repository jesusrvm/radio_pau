<div class="left-side sticky-left-side">

    <!--logo and iconic logo start-->
    <div class="logo">
        <h1><a href="index.php">con<span>M</span>de memoria</a></h1>
    </div>
    <div class="logo-icon text-center">
        <a href="index.php">M </a>
    </div>
<!-- /w3l-agile -->
    <!--logo and iconic logo end-->
    <div class="left-side-inner">

        <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li class="active"><a href="index.php"><i class="lnr lnr-home"></i><span>Inicio</span></a></li>
                <li><a href="radio.php"><i class="lnr lnr-music-note"></i><span>Programas</span></a></li>
                <li><a href="#" data-toggle="modal" data-target="#myModal1"><i class="camera"></i><span>En vivo</span></a></li>
                <!--<li><a href="radio.php"><i class="lnr lnr-users"></i> <span>Artists</span></a></li> 
                <li><a href="browse.php"><i class="lnr lnr-music-note"></i> <span>Albums</span></a></li>						
                <li class="menu-list"><a href="browse.php"><i class="lnr lnr-indent-increase"></i> <span>Browser</span></a>  
                    <ul class="sub-menu-list">
                        <li><a href="browse.php">Artists</a> </li>
                        <li><a href="404.php">Services</a> </li>
                    </ul>
                </li>-->
                <li><a href="blog.php"><i class="lnr lnr-book"></i><span>Blog</span></a></li>
                <!--<li><a href="typography.php"><i class="lnr lnr-pencil"></i> <span>Typography</span></a></li>
                <li class="menu-list"><a href="#"><i class="lnr lnr-heart"></i>  <span>My Favourities</span></a> 
                    <ul class="sub-menu-list">
                        <li><a href="radio.php">All Songs</a></li>
                    </ul>
                </li>-->
                
                <li><a href="contact.php"><i class="fa fa-thumb-tack"></i><span>Contacto</span></a>
                </li>
                
                
            </ul>
        <!--sidebar nav end-->
    </div>
</div>